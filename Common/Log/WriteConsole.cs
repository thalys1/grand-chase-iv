﻿using System;

namespace Commons.Log
{
    public static class WriteConsole
    {
        private static readonly object Lock = new object();

        public static void Print(string message)
        {
            lock (Lock)
            {
                Console.WriteLine(message);
            }
        }

        public static void Info(string message)
        {
            lock (Lock)
            {
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write($"{DateTime.Now:yyyy-MM-dd HH:mm:ss}");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(" - ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("INFO ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(message);
                Console.WriteLine();
            }
        }

        public static void Server(string message)
        {
            lock (Lock)
            {
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}]");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(" [ SERVER ] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(message);
                Console.WriteLine();
            }
        }

        public static void Warning(string message)
        {
            lock (Lock)
            {
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write($"[{DateTime.Now:yyyy-MM-dd HH:mm:ss}]");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(" [ WARNING ] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(message);
                Console.WriteLine();
            }
        }
    }
}