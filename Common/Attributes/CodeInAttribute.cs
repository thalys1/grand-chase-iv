﻿using System;

namespace Commons.Attributes
{
    public class CodeInAttribute : Attribute
    {
        public CodeInAttribute(int code)
        {
            Code = code;
        }

        public CodeInAttribute(object code)
        {
            Code = (int) code;
        }

        public int Code { get; }
    }
}