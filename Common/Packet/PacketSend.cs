﻿using System;

namespace Commons.Packet
{
    public abstract class PacketSend : IDisposable
    {
        protected PacketSend(short code)
        {
            Code = code;
            Write = new PacketWrite();
        }

        public short Code { get; }

        public PacketWrite Write { get; }
        
        public void Dispose()
        {
            Write.Dispose();
        }
    }
}