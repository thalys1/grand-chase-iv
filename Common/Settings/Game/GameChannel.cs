using System;
using System.Xml.Serialization;

namespace Commons.Settings.Game
{
    [Serializable]
    public class GameChannel
    {
        [XmlElement("ChannelID")] public int ChannelId { get; set; }

        [XmlElement("Name")] public string Name { get; set; }

        [XmlElement("Limit")] public int Limit { get; set; }

        [XmlElement("Level")] public int Level { get; set; }
    }
}