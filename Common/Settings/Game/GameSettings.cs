using System;
using System.Xml.Serialization;
using Commons.Settings.Database;
using Commons.Settings.Network;

namespace Commons.Settings.Game
{
    [Serializable]
    public class GameSettings
    {
        public GameSettings()
        {
            ServerId = 1;
            UdpRelay = 9300;
            TcpRelay = 9300;
            Network = new NetworkSettings
            {
                Host = "*",
                Port = 9500,
                Backlog = 100,
                MaxConnections = 100,
                BufferSize = 65536,
                MaxSimultaneousAcceptOps = 512,
                NumOfSaeaForRec = 256,
                NumOfSaeaForSend = 256
            };
            Database = new SessionSettings
            {
                Server = "127.0.0.1",
                Username = "root",
                Password = "123456",
                Database = "gc"
            };
            Channels = new[]
            {
                new GameChannel
                {
                    ChannelId = 1,
                    Name = "대전",
                    Limit = 1500,
                    Level = 1
                },
                new GameChannel
                {
                    ChannelId = 6,
                    Name = "던전",
                    Limit = 1500,
                    Level = 1
                }
            };
        }

        [XmlElement("ServerID")] public int ServerId { get; set; }

        [XmlElement("UDPRelayPort")] public int UdpRelay { get; set; }

        [XmlElement("TCPRelayPort")] public int TcpRelay { get; set; }

        [XmlElement("Network")] public NetworkSettings Network { get; set; }

        [XmlElement("Database")] public SessionSettings Database { get; set; }

        [XmlArray("Channels")]
        [XmlArrayItem(typeof(GameChannel), ElementName = "Channel")]
        public GameChannel[] Channels { get; set; }
    }
}