﻿using System;
using System.Xml.Serialization;

namespace Commons.Settings.Database
{
    [Serializable]
    public class SessionSettings
    {
        [XmlElement("Server")] public string Server { get; set; }

        [XmlElement("Username")] public string Username { get; set; }

        [XmlElement("Password")] public string Password { get; set; }

        [XmlElement("Database")] public string Database { get; set; }
    }
}