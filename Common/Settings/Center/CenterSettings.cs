using System;
using System.Xml.Serialization;
using Commons.Settings.Database;
using Commons.Settings.Network;

namespace Commons.Settings.Center
{
    [Serializable]
    public class CenterSettings
    {
        public CenterSettings()
        {
            Network = new NetworkSettings
            {
                Host = "*",
                Port = 9501,
                Backlog = 100,
                MaxConnections = 100,
                BufferSize = 65536,
                MaxSimultaneousAcceptOps = 512,
                NumOfSaeaForRec = 256,
                NumOfSaeaForSend = 256
            };
            Database = new SessionSettings
            {
                Server = "127.0.0.1",
                Username = "root",
                Password = "123456",
                Database = "gc"
            };
        }

        [XmlElement("Network")] public NetworkSettings Network { get; set; }

        [XmlElement("Database")] public SessionSettings Database { get; set; }
    }
}