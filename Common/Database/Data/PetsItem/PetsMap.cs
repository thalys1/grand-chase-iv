using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.PetsItem
{
    [Serializable]
    public class PetsMap : ClassMap<PetsModel>
    {
        private PetsMap()
        {
            Table("pets_item");
            Id(x => x.Id).Column("id");
            Map(x => x.ItemId).Column("item_id");
            Map(x => x.PetId).Column("pet_id");
            Map(x => x.Promotion).Column("promotion");
        }
    }
}