namespace Commons.Database.Data.PetsItem
{
    public class PetsModel
    {
        public virtual int Id { get; protected set; }
        public virtual int ItemId { get; protected set; }
        public virtual int PetId { get; protected set; }
        public virtual int Promotion { get; protected set; }
    }
}