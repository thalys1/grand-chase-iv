namespace Commons.Database.Data.UsersSage
{
    public class UsersStageModel
    {
        public virtual int Id { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual int ModeId { get; protected set; }
        public virtual int DifficultyLevelOne { get; protected set; }
        public virtual int DifficultyLevelTwo { get; protected set; }
        public virtual int DifficultyLevelThree { get; protected set; }
    }
}