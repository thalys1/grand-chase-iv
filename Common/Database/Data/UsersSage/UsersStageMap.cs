using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersSage
{
    [Serializable]
    public class UsersStageMap : ClassMap<UsersStageModel>
    {
        private UsersStageMap()
        {
            Table("users_stage");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.ModeId).Column("mode_id");
            Map(x => x.DifficultyLevelOne).Column("difficulty_level_one");
            Map(x => x.DifficultyLevelTwo).Column("difficulty_level_two");
            Map(x => x.DifficultyLevelThree).Column("difficulty_level_three");
        }
    }
}