﻿namespace Commons.Database.Data.UsersInventory
{
    public class UsersInventoryModel
    {
        public virtual int Id { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual int ItemId { get; protected set; }
        public virtual int Type { get; protected set; }
        public virtual int Quantity { get; protected set; }
    }
}