﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersInventory
{
    [Serializable]
    public class UsersInventoryMap : ClassMap<UsersInventoryModel>
    {
        private UsersInventoryMap()
        {
            Table("users_inventory");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.ItemId).Column("item_id");
            Map(x => x.Type).Column("type");
            Map(x => x.Quantity).Column("quantity");
        }
    }
}