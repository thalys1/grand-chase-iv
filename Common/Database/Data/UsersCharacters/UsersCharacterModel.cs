﻿using System.Collections.Generic;
using Commons.Database.Data.UsersCharactersSkillTree;

namespace Commons.Database.Data.UsersCharacters
{
    public class UsersCharacterModel
    {
        public virtual int Id { get; set; }
        public virtual int UserId { get; set; }
        public virtual int CharacterType { get; set; }
        public virtual int Level { get; set; }
        public virtual int Experience { get; set; }
        public virtual int Pet { get; set; }
        public virtual int Wins { get; set; }
        public virtual int Losses { get; set; }
        public virtual int SP { get; set; }
        public virtual int Promotion { get; set; }
        public virtual int SecondWeapon { get; set; }
        public virtual int SecondWeaponId { get; set; }
        public virtual int SecondWeaponUId { get; set; }
        public virtual IList<UsersCharacterSkillTreeModel> Skills { get; }
    }
}