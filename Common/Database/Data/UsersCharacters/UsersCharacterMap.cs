﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersCharacters
{
    [Serializable]
    public class UsersCharacterMap : ClassMap<UsersCharacterModel>
    {
        private UsersCharacterMap()
        {
            Table("users_characters");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.CharacterType).Column("character_type");
            Map(x => x.Level).Column("level").Default("1");
            Map(x => x.Experience).Column("experience").Default("0");
            Map(x => x.Pet).Column("pet").Default("0");
            Map(x => x.Wins).Column("wins").Default("0");
            Map(x => x.Losses).Column("losses").Default("0");
            Map(x => x.SP).Column("sp").Default("0");
            Map(x => x.Promotion).Column("promotion").Default("0");
            Map(x => x.SecondWeapon).Column("second_weapon").Default("0");
            Map(x => x.SecondWeaponId).Column("second_weapon_id").Default("0");
            Map(x => x.SecondWeaponUId).Column("second_weapon_uid").Default("0");
            HasMany(x => x.Skills).KeyColumn("user_id").Not.LazyLoad();
        }
    }
}