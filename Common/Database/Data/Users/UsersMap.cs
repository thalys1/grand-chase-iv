﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.Users
{
    [Serializable]
    public class UsersMap : ClassMap<UsersModel>
    {
        private UsersMap()
        {
            Table("users");
            Id(x => x.Id).Column("id");
            Map(x => x.Username).Column("username");
            Map(x => x.Password).Column("password");
            Map(x => x.Gender).Column("gender");
            Map(x => x.Online).Column("online");
            Map(x => x.AuthLevel).Column("auth_level");
            Map(x => x.InventorySize).Column("inventory_size");
            Map(x => x.LifeBonus).Column("life_bonus");
            Map(x => x.GamePoints).Column("game_points");
            Map(x => x.VirtualCash).Column("virtual_cash");
            Map(x => x.GuildId).Column("guild_id");
            HasMany(x => x.Characters).KeyColumn("user_id").Not.LazyLoad();
            HasMany(x => x.Stages).KeyColumn("user_id").Not.LazyLoad();
            HasMany(x => x.Quests).KeyColumn("user_id").Not.LazyLoad();
        }
    }
}