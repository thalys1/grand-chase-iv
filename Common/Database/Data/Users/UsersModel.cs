﻿using System.Collections.Generic;
using Commons.Database.Data.UsersCharacters;
using Commons.Database.Data.UsersQuests;
using Commons.Database.Data.UsersSage;

namespace Commons.Database.Data.Users
{
    public class UsersModel
    {
        public virtual int Id { get; protected set; }
        public virtual string Username { get; protected set; }
        public virtual string Password { get; protected set; }
        public virtual byte Gender { get; protected set; }
        public virtual int Online { get; protected set; }
        public virtual int AuthLevel { get; protected set; }
        public virtual int InventorySize { get; protected set; }
        public virtual int LifeBonus { get; protected set; }
        public virtual int GamePoints { get; protected set; }
        public virtual int VirtualCash { get; protected set; }
        public virtual int GuildId { get; protected set; }

        public virtual IList<UsersCharacterModel> Characters { get; }
        public virtual IList<UsersStageModel> Stages { get; }
        public virtual IList<UsersQuestModel> Quests { get; }
    }
}