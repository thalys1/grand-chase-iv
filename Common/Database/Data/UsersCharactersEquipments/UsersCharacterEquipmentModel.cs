﻿namespace Commons.Database.Data.UsersCharactersEquipments
{
    public class UsersCharacterEquipmentModel
    {
        public virtual int Id { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual int ItemUId { get; protected set; }
        public virtual int ItemId { get; protected set; }
        public virtual int CharType { get; protected set; }
    }
}