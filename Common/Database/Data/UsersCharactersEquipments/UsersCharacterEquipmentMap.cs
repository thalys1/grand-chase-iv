﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersCharactersEquipments
{
    [Serializable]
    public class UsersCharacterEquipmentMap : ClassMap<UsersCharacterEquipmentModel>
    {
        private UsersCharacterEquipmentMap()
        {
            Table("users_characters_equipments");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.ItemUId).Column("item_uid");
            Map(x => x.ItemId).Column("item_id");
            Map(x => x.CharType).Column("char_type");
        }
    }
}