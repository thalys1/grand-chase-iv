namespace Commons.Database.Data.UsersQuests
{
    public class UsersQuestModel
    {
        public virtual int Id { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual int QuestId { get; protected set; }
        public virtual int QuestUId { get; protected set; }
        public virtual int Progress { get; protected set; }
    }
}