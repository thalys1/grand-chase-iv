using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersQuests
{
    [Serializable]
    public class UsersQuestMap : ClassMap<UsersQuestModel>
    {
        private UsersQuestMap()
        {
            Table("users_quests");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.QuestId).Column("quest_id");
            Map(x => x.QuestUId).Column("quest_uid");
            Map(x => x.Progress).Column("progress");
        }
    }
}