﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.UsersCharactersSkillTree
{
    [Serializable]
    public class UsersCharacterSkillTreeMap : ClassMap<UsersCharacterSkillTreeModel>
    {
        private UsersCharacterSkillTreeMap()
        {
            Table("users_characters_skill_tree");
            Id(x => x.Id).Column("id");
            Map(x => x.UserId).Column("user_id");
            Map(x => x.CharType).Column("char_type");
            Map(x => x.Promotion).Column("promotion");
            Map(x => x.SetId).Column("set_id");
            Map(x => x.SkillId).Column("skill_id");
        }
    }
}