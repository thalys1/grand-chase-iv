﻿namespace Commons.Database.Data.UsersCharactersSkillTree
{
    public class UsersCharacterSkillTreeModel
    {
        public virtual int Id { get; protected set; }
        public virtual int UserId { get; protected set; }
        public virtual int CharType { get; protected set; }
        public virtual int Promotion { get; protected set; }
        public virtual int SetId { get; protected set; }
        public virtual int SkillId { get; protected set; }
    }
}