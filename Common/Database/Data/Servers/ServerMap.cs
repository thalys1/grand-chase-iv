﻿using System;
using FluentNHibernate.Mapping;

namespace Commons.Database.Data.Servers
{
    [Serializable]
    public class ServerMap : ClassMap<ServerModel>
    {
        private ServerMap()
        {
            Table("servers");
            Id(x => x.Id).Column("id");
            Map(x => x.Name).Column("name");
            Map(x => x.Description).Column("description");
            Map(x => x.Type).Column("server_type");
            Map(x => x.Address).Column("server_address");
            Map(x => x.Welcome).Column("server_welcome");
            Map(x => x.Port).Column("server_port");
            Map(x => x.RelayPort).Column("server_relay_port");
            Map(x => x.Users).Column("users_online");
            Map(x => x.Limit).Column("limit_online");
            Map(x => x.Level).Column("level");
            Map(x => x.Flag).Column("flag");
        }
    }
}