﻿namespace Commons.Database.Data.Servers
{
    public class ServerModel
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; protected set; }
        public virtual string Description { get; protected set; }
        public virtual string Address { get; protected set; }
        public virtual string Welcome { get; protected set; }
        public virtual int Type { get; protected set; }
        public virtual int Port { get; protected set; }
        public virtual int RelayPort { get; protected set; }
        public virtual int Users { get; protected set; }
        public virtual int Limit { get; protected set; }
        public virtual int Level { get; protected set; }
        public virtual int Flag { get; protected set; }
    }
}