﻿using Commons.Database.Data.PetsItem;
using Commons.Database.Data.Servers;
using Commons.Database.Data.Users;
using Commons.Database.Data.UsersCharacters;
using Commons.Database.Data.UsersCharactersEquipments;
using Commons.Database.Data.UsersCharactersSkillTree;
using Commons.Database.Data.UsersInventory;
using Commons.Database.Data.UsersQuests;
using Commons.Database.Data.UsersSage;
using Commons.Settings.Database;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace Commons.Database
{
    public static class DatabaseFactory
    {
        public static ISessionFactory Session { get; private set; }

        public static bool Build(SessionSettings settings, int action = 1)
        {
            try
            {
                var configuration = GetConfig(MySql(settings));
                switch (action)
                {
                    case 1:
                        new SchemaUpdate(configuration).Execute(false, true);
                        break;
                    case 2:
                        new SchemaExport(configuration).Execute(false, false, true);
                        break;
                }

                Session = configuration.BuildSessionFactory();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static Configuration GetConfig(IPersistenceConfigurer db)
        {
            return Fluently.Configure()
                .Database(db)
                .Cache(c => c.UseQueryCache().UseSecondLevelCache().UseMinimalPuts())
                .Mappings(m => m.FluentMappings
                    .Add<UsersCharacterMap>()
                    .Add<UsersCharacterEquipmentMap>()
                    .Add<UsersCharacterSkillTreeMap>()
                    .Add<UsersInventoryMap>()
                    .Add<UsersMap>()
                    .Add<UsersStageMap>()
                    .Add<UsersQuestMap>()
                    .Add<ServerMap>()
                    .Add<PetsMap>())
                .BuildConfiguration();
        }

        private static IPersistenceConfigurer MySql(SessionSettings settings)
        {
            return MySQLConfiguration.Standard
                .ConnectionString(x =>
                    x.Server(settings.Server)
                        .Username(settings.Username)
                        .Password(settings.Password)
                        .Database(settings.Database)
                );
        }
    }
}