﻿using System;
using Networking.Api.Buffer;
using Networking.Api.Channels;
using Networking.Api.Context.Channels;
using Networking.Api.Pipeline.Handlers;

namespace Networking.Api.Pipeline
{
    public class NetworkingPipeline : IChannelHandler
    {
        private IChannelHandler Handler { get; set; }

        public void Set(IChannelHandler handler)
        {
            Handler = handler;
        }

        public void OnChannelConnected(ChannelHandlerContext context)
        {
            Handler.OnChannelConnected(context);
        }

        public void OnChannelDisconnected(ChannelHandlerContext context)
        {
            Handler.OnChannelDisconnected(context);
        }

        public void OnChannelEvent(ChannelEvent triggeredEvent, ChannelHandlerContext context)
        {
            if (!triggeredEvent.IsComplete)
            {
                Handler.OnChannelEvent(triggeredEvent, context);
            }
        }

        public void OnChannelException(Exception exception, ChannelHandlerContext context)
        {
            Handler.OnChannelException(exception, context);
        }

        public void OnChannelDataReceived(IBuffer buffer, ChannelHandlerContext context)
        {
            if(!buffer.IsReadable()) return;
            Handler.OnChannelDataReceived(buffer, context);
        }
    }
}
