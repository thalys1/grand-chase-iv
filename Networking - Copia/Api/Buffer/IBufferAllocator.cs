﻿namespace Networking.Api.Buffer
{
    public interface IBufferAllocator
    {
        bool Alloc(int size, out IBuffer buffer);

        void Dealloc(IBuffer buffer);
    }
}
