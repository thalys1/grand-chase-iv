﻿using Networking.Api.Channels;

namespace Networking.Api.Context.Channels
{
    public class ChannelHandlerContext : INetworkingContext
    {
        private Channel _channel;

        public Channel Channel
        {
            get
            {
                return this._channel;
            }
        }

        public ChannelHandlerContext(Channel channel)
        {
            this._channel = channel;
        }
    }
}
