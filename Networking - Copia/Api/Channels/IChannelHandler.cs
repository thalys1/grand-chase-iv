﻿using System;
using Networking.Api.Buffer;
using Networking.Api.Channels;
using Networking.Api.Context.Channels;

namespace Networking.Api.Pipeline.Handlers
{
    public interface IChannelHandler
    {
        void OnChannelConnected(ChannelHandlerContext context);

        void OnChannelDisconnected(ChannelHandlerContext context);

        void OnChannelDataReceived(IBuffer buffer, ChannelHandlerContext context);

        void OnChannelEvent(ChannelEvent triggeredEvent, ChannelHandlerContext context);

        void OnChannelException(Exception exception, ChannelHandlerContext context);
    }
}
