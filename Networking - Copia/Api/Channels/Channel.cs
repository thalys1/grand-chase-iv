﻿using Networking.Api.Buffer;
using Networking.Api.Context.Channels;
using Networking.Api.Pipeline;

namespace Networking.Api.Channels
{
    public abstract class Channel
    {
        private int _id;
        private NetworkingPipeline _pipeline = new NetworkingPipeline();

        public int Id
        {
            get
            {
                return this._id;
            }

            protected set
            {
                this._id = Id;
            }
        }

        public NetworkingPipeline Pipeline
        {
            get
            {
                return this._pipeline;
            }
        }

        public string IpAddress
        {
            get
            {
                return this.GetIpAddress();
            }
        }

        public abstract void Write(IBuffer buffer);

        protected abstract string GetIpAddress();

        public void FireEvent(ChannelEvent channelEvent)
        {
            this._pipeline.OnChannelEvent(channelEvent, new ChannelHandlerContext(this));
        }

        public abstract void Close();

        public void Dispose()
        {
            this.OnDispose();
        }

        public virtual void OnDispose()
        {
            // do we need to do anything else in our own channel implementations?
        }
    }
}
