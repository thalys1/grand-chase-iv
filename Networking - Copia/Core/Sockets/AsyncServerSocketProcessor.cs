﻿using System;
using System.Net.Sockets;
using System.Text;
using Commons.Log;
using Networking.Api.Buffer.Default;
using Networking.Api.Context.Channels;
using Networking.Core.Channels;

namespace Networking.Core.Sockets
{
    partial class AsyncServerSocket
    {
        /// <summary>
        /// Processes an accept operation
        /// </summary>
        /// <param name="acceptEventArgs"></param>
        private void ProcessAccept(SocketAsyncEventArgs acceptEventArgs)
        {
            this.StartAccept();

            if(acceptEventArgs.SocketError != SocketError.Success)
            {
                this.CancelAccept(acceptEventArgs);
                return;
            }

            SocketAsyncEventArgs ioArgs = this._ioArgsPool.Take();
            
            if(ioArgs != null)
            {
                ChannelToken channelToken = ioArgs.UserToken as ChannelToken;
                DefaultChannel channel = channelToken.Channel as DefaultChannel;

                channel.Socket = acceptEventArgs.AcceptSocket;
                channel.SendArgs.AcceptSocket = acceptEventArgs.AcceptSocket;

                acceptEventArgs.AcceptSocket = null;
                this._acceptArgsPool.Return(acceptEventArgs);
            
                StartReceive(ioArgs);

                channel.Pipeline.OnChannelConnected(new ChannelHandlerContext(channel));
            }
        }

        /// <summary>
        /// Processes a receive operation
        /// </summary>
        /// <param name="receiveEventArgs">The arguments which correspond to the receive operation</param>
        private void ProcessReceive(SocketAsyncEventArgs receiveEventArgs)
        {
            ChannelToken channelToken = receiveEventArgs.UserToken as ChannelToken;

            if (receiveEventArgs.BytesTransferred > 0 && receiveEventArgs.SocketError == SocketError.Success)
            {
                byte[] dataReceived = new byte[receiveEventArgs.BytesTransferred];

                Buffer.BlockCopy(receiveEventArgs.Buffer, receiveEventArgs.Offset, dataReceived, 0, receiveEventArgs.BytesTransferred);

                WriteConsole.Server($"Received buffer {Encoding.UTF8.GetString(dataReceived)}");

                channelToken.Channel.Pipeline.OnChannelDataReceived(
                    new DefaultBuffer(dataReceived, dataReceived.Length, 0),
                    new ChannelHandlerContext(channelToken.Channel));

                this.StartReceive(receiveEventArgs);
            }
            else
            {
                // Disconnect socket!

                channelToken.Channel.Pipeline.OnChannelDisconnected(new ChannelHandlerContext(channelToken.Channel));
                channelToken.Channel.Dispose();

                this.CancelReceive(receiveEventArgs);
            }
        }

        /// <summary>
        /// Processes a flush operation
        /// </summary>
        /// <param name="flushArgs">The arguments which correspond to the flush operation</param>
        private void ProcessFlush(SocketAsyncEventArgs flushArgs)
        {
            ChannelToken channelToken = flushArgs.UserToken as ChannelToken;

            // We're ready to send
            if (flushArgs.SocketError == SocketError.Success)
            {
                channelToken.DataWriter.DataRemaining = channelToken.DataWriter.DataRemaining - flushArgs.BytesTransferred;

                if (channelToken.DataWriter.DataRemaining == 0)
                {
                    channelToken.DataWriter.Reset();
                    (channelToken.Channel as DefaultChannel).OnFlushComplete();
                }
                else
                {
                    channelToken.DataWriter.DataProcessed += flushArgs.BytesTransferred;
                    this.StartFlush(flushArgs);
                }
            }
        }
    }
}
