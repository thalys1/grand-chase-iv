﻿using System;
using System.Net.Sockets;
using Commons.Collections;

namespace Networking.Core.Sockets
{
    class SocketAsyncEventArgsPool : QueuedObjectPool<SocketAsyncEventArgs>
    {
        public SocketAsyncEventArgsPool(int size, Func<SocketAsyncEventArgs> factory)
        {
            this.Initialise(size, size, factory);
        }
    }
}
