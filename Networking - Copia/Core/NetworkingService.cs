﻿using System;
using System.Net;
using Networking.Api;
using Networking.Api.Channels;
using Networking.Api.Context;
using Networking.Core.Sockets;

namespace Networking.Core
{
    public class NetworkingService : INetworkingService
    {
        private IChannelInitialiser _channelInitialiser;

        private String _hostName;
        private int _port;

        private AsyncServerSocket _serverSocket;

        public void Configure(IChannelInitialiser channelInitialiser)
        {
            this._channelInitialiser = channelInitialiser;
        }

        public void Start(string hostName, int port, Action<ServiceInitialisedContext> onServiceInitialised)
        {
            this._hostName = hostName;
            this._port = port;

            if (!IPAddress.TryParse(this._hostName, out IPAddress ipAddress))
            {
                throw new InvalidOperationException("Invalid host name detected, a valid IP address is required");
            }

            this._serverSocket = new AsyncServerSocket(new IPEndPoint(ipAddress, port), this._channelInitialiser);

            this._serverSocket.Listen();

            onServiceInitialised.Invoke(new ServiceInitialisedContext(this));
        }
    }
}
