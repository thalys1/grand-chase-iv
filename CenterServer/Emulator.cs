﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using CenterServer.Communication.Handler;
using CenterServer.Utilities;
using Commons.Database;
using Commons.Log;
using Commons.Settings;
using Commons.Settings.Center;
using Networking;
using Networking.Packet;

namespace CenterServer
{
    public static class Emulator
    {
        private static readonly XmlSerializer<CenterSettings> Settings = new XmlSerializer<CenterSettings>();

        public static void Main(string[] args)
        {
            Console.Title = "Loading Center Server...";
            WriteConsole.Info("Loading server...");
            if (Settings.Load(Path.Combine(Application.StartupPath, @"CenterServer.xml")))
            {
                if (DatabaseFactory.Build(Settings.Data.Database))
                {
                    PacketManager.Configure(new RequestHandler());

                    NetworkServer.Configure(new ChannelHandler(), Settings.Data.Network);
                    WriteConsole.Info($"Server started on {Settings.Data.Network.Host}:{Settings.Data.Network.Port}");
                    TitleTicker.Build();
                    NetworkServer.Start();
                }
                else
                {
                    WriteConsole.Info("Can't connect to database..");
                }
            }
            else
            {
                WriteConsole.Info("Can't load configurations.");
            }

            while (true) Thread.Sleep(50);
        }
    }
}