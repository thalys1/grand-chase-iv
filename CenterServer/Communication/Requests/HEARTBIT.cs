﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Requests
{
    [CodeIn(Opcodes.HEART_BIT_NOT)]
    public class HEARTBIT : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.Int(0);
                sessionChannel.Send(packetWrite, 0);
            }
        }
    }
}