﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Requests
{
    [CodeIn(Opcodes.ENU_GUIDE_BOOK_LIST_REQ)]
    public class GUIDE_BOOK_LIST_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 00 00 01 00 00 00 00 00 00 00");
                sessionChannel.Send(packetWrite, (short) Opcodes.ENU_GUIDE_BOOK_LIST_ACK);
            }
        }
    }
}