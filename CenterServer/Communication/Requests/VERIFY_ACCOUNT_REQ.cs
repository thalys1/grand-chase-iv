﻿using CenterServer.Communication.Send;
using Commons.Attributes;
using Commons.Database;
using Commons.Database.Data.Servers;
using Commons.Database.Data.Users;
using Commons.Log;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Requests
{
    [CodeIn(Opcodes.ENU_VERIFY_ACCOUNT_REQ)]
    public class VERIFY_ACCOUNT_REQ : IPacketRequest
    {
        // 9 = incorrect
        // 14 = region blocked
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            var username = packetRead.String();
            var password = packetRead.String();
            WriteConsole.Server($"VERIFY ACCOUNT: {username}:{password}");
            try
            {
                using (var openSession = DatabaseFactory.Session.OpenSession())
                {
                    using (var transaction = openSession.BeginTransaction())
                    {
                        var userModel = openSession.QueryOver<UsersModel>().Where(x => x.Username == username)
                            .Where(x => x.Password == password).SingleOrDefault();
                        if (userModel == null)
                        {
                            using (var packetWrite = new PacketWrite())
                            {
                                packetWrite.Int(9);
                                packetWrite.UnicodeStr(username);
                                packetWrite.Int(0);
                                sessionChannel.Send(packetWrite, (short) Opcodes.ENU_VERIFY_ACCOUNT_ACK);
                            }

                            return;
                        }

                        sessionChannel.Send(new SERVER_LIST_NOT(openSession.QueryOver<ServerModel>().List()));
                        sessionChannel.Send(new CHANNEL_NEWS_NOT());
                        sessionChannel.Send(new NEW_CLIENT_CONTENTS_OPEN_NOT());
                        sessionChannel.Send(new SOCKET_TABLE_INFO_NOT());
                        sessionChannel.Send(new CASHBACK_RATIO_INFO_NOT());

                        using (var packetWrite = new PacketWrite())
                        {
                            packetWrite.Int(0);
                            packetWrite.UnicodeStr(username);
                            packetWrite.Str(password);
                            packetWrite.Byte(0);
                            packetWrite.HexArray(
                                "00 00 00 14 00 8E A7 C5 01 00 00 00 00 00 00 02 4B 52 00 05 A3 BD 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 03 00 00 00 00 00 00 00 00 00 00 00 00");
                            packetWrite.UnicodeStr("http://192.95.4.5/GuildMarks/");
                            packetWrite.HexArray(
                                "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 03 00 00 00 00 00 00 00 00 64 01 00 00 00 00 00 00 00 64 02 00 00 00 00 00 00 00 64 01 BF 80 00 00 FC 04 97 FF 00 00 00 00 00 00 00 00 00 00 00 00 00");
                            sessionChannel.Send(packetWrite, (short) Opcodes.ENU_VERIFY_ACCOUNT_ACK);
                        }

                        transaction.Commit();
                    }
                }
            }
            catch
            {
                using (var packetWrite = new PacketWrite())
                {
                    packetWrite.Int(16);
                    packetWrite.UnicodeStr(username);
                    packetWrite.Int(0);
                    sessionChannel.Send(packetWrite, (short) Opcodes.ENU_VERIFY_ACCOUNT_ACK);
                }
            }
        }
    }
}