﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Requests
{
    [CodeIn(Opcodes.ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_REQ)]
    public class CLIENT_CONTENTS_FIRST_INIT_INFO_REQ : IPacketRequest
    {
        private readonly string[] _loadsStrings = {"Load1_1.dds", "Load1_2.dds", "Load1_3.dds", "LoadGauge.dds"};
        private readonly string[] _matchStrings = {"ui_match_load1.dds", "ui_match_load2.dds", "ui_match_load3.dds"};
        private readonly string[] _squareStrings = {"Square.lua", "SquareObject.lua", "Square3DObject.lua"};

        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 01 00 00 00");
                packetWrite.HexArray("02 00 00 00 03 00 00 00 04 00 00 00 01 00 00 00 00");

                packetWrite.Int(_loadsStrings.Length);
                for (var i = 0; i <= _loadsStrings.Length - 1; i++)
                    packetWrite.UnicodeStr(_loadsStrings[i]);
                packetWrite.HexArray("00 00 00 02 00 00 00 00 00 00 00 01 00 00 00 01 00 00 00 00");

                packetWrite.Int(_matchStrings.Length);
                for (var i = 0; i <= _matchStrings.Length - 1; i++)
                    packetWrite.UnicodeStr(_matchStrings[i]);
                packetWrite.Int(0);

                packetWrite.Int(_squareStrings.Length);
                for (var i = 0; i <= _squareStrings.Length - 1; i++)
                {
                    packetWrite.UnicodeStr(_squareStrings[i]);
                }

                packetWrite.HexArray("00 00 00 03 00 00 00 00 00 00 00 01 00 00 00 02 00 00 00 00");
                packetWrite.Int(0);
                sessionChannel.Send(packetWrite, (short) Opcodes.ENU_CLIENT_CONTENTS_FIRST_INIT_INFO_ACK);
            }
        }
    }
}