﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Requests
{
    [CodeIn(Opcodes.ENU_SHAFILENAME_LIST_REQ)]
    public class CLIENT_SHARFILE_LIST_REQ : IPacketRequest
    {
        private readonly string[] _fileList = {"ai.kom", "main.exe", "script.kom"};

        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.Int(0);
                packetWrite.Int(_fileList.Length);
                for (var i = 0; i <= _fileList.Length - 1; i++)
                {
                    packetWrite.Int(_fileList[i].Length * 2);
                    packetWrite.UnicodeStr(_fileList[i]);
                }

                sessionChannel.Send(packetWrite, (short) Opcodes.ENU_SHAFILENAME_LIST_ACK);
            }
        }
    }
}