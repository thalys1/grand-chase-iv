﻿using Commons.Packet;
using Networking.Packet;

namespace CenterServer.Communication.Send
{
    public class CASHBACK_RATIO_INFO_NOT : PacketSend
    {
        public CASHBACK_RATIO_INFO_NOT() : base((short) Opcodes.ENU_CASHBACK_RATIO_INFO_NOT)
        {
            Write.HexArray("00 00 00 00 00 00 00 00 00 00 00");
        }
    }
}