using Commons.Packet;
using Networking.Packet;

namespace CenterServer.Communication.Send
{
    public class WAIT_TIME_NOT : PacketSend
    {
        public WAIT_TIME_NOT(int time) : base((short) Opcodes.ENU_WAIT_TIME_NOT)
        {
            Write.Int(time);
        }
    }
}