﻿using System.Collections.Generic;
using Commons.Database.Data.Servers;
using Commons.Packet;
using Networking.Packet;

namespace CenterServer.Communication.Send
{
    public class SERVER_LIST_NOT : PacketSend
    {
        public SERVER_LIST_NOT(ICollection<ServerModel> servers) : base((short) Opcodes.ENU_SERVER_LIST_NOT)
        {
            Write.Int(servers.Count);
            foreach (var server in servers)
            {
                Write.Int(server.Id);
                Write.Int(server.Id);
                Write.UnicodeStr(server.Name);
                Write.Str(server.Address);
                Write.Short((short) server.Port);
                Write.Int(server.Users);
                Write.Int(server.Limit);
                Write.Int(server.Flag);
                Write.Int(-1);
                Write.Int(-1);
                Write.Str(server.Address);
                Write.UnicodeStr(server.Description);
                Write.Int(server.Type);
            }
        }
    }
}