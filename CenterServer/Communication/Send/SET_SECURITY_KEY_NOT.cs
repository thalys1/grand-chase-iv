using Commons.Packet;
using Networking.Packet;

namespace CenterServer.Communication.Send
{
    public class SET_SECURITY_KEY_NOT : PacketSend
    {
        public SET_SECURITY_KEY_NOT(byte[] newCryptoAuth, byte[] newCryptoKey) : base((short) Opcodes.SET_SECURITY_KEY_NOT)
        {
            Write.Short(24787);
            Write.Int(8);
            Write.ArrayBytes(newCryptoAuth);
            Write.Int(8);
            Write.ArrayBytes(newCryptoKey);
            Write.Int(1);
            Write.Int(0);
            Write.Int(0);
        }
    }
}