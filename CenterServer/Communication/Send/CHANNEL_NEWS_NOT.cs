﻿using Commons.Packet;
using Networking.Packet;

namespace CenterServer.Communication.Send
{
    public class CHANNEL_NEWS_NOT : PacketSend
    {
        public CHANNEL_NEWS_NOT() : base((short) Opcodes.ENU_CHANNEL_NEWS_NOT)
        {
            Write.HexArray("00 00 00 00 00 00 00 00 01 00 00");
        }
    }
}