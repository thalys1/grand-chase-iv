using System;
using System.Threading;
using CenterServer.Communication.Send;
using Commons.Log;
using Commons.Packet;
using Networking;
using Networking.Channels;
using Networking.Packet;

namespace CenterServer.Communication.Handler
{
    internal class ChannelHandler : INetworkHandler
    {

        public void OnChannelConnected(SessionChannel sessionChannel)
        {
            var cryptography = sessionChannel.Cryptography;
            var cryptoKey = cryptography.RNGCryptoServiceProvider();
            var cryptoAuth = cryptography.RNGCryptoServiceProvider();
            sessionChannel.Send(new SET_SECURITY_KEY_NOT(cryptoAuth, cryptoKey));
            cryptography.AuthenticKey = cryptoAuth;
            cryptography.CryptographyKey = cryptoKey;
            sessionChannel.Send(new WAIT_TIME_NOT(100));
        }

        public void OnChannelDataReceived(int packetId, PacketRead packetRead, SessionChannel sessionChannel)
        {
            NetworkManager.Add(sessionChannel);
            if (PacketManager.TryRequest(packetId, out var packetRequest))
                packetRequest.Dispatch(sessionChannel, packetRead);
            else
                WriteConsole.Warning($"Packet not found with id: {packetId}");

            WriteConsole.Info($"Thread: {Thread.CurrentThread.ManagedThreadId}");
        }

        public void OnChannelDisconnected(SessionChannel sessionChannel)
        {
            NetworkManager.Remove(sessionChannel);
            WriteConsole.Info("A channel disconnected");
        }

        public void OnChannelException(Exception exception, SessionChannel sessionChannel)
        {
            WriteConsole.Warning($"A channel exception was caught {exception}");
        }
    }
}