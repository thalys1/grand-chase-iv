using CenterServer.Communication.Requests;
using Networking.Packet;

namespace CenterServer.Communication.Handler
{
    public class RequestHandler : PacketHandler
    {
        public RequestHandler()
        {
            Register(new CLIENT_CONTENTS_FIRST_INIT_INFO_REQ());
            Register(new CLIENT_PING_CONFIG_REQ());
            Register(new CLIENT_SHARFILE_LIST_REQ());
            Register(new GUIDE_BOOK_LIST_REQ());
            Register(new HEARTBIT());
            Register(new VERIFY_ACCOUNT_REQ());
        }
    }
}