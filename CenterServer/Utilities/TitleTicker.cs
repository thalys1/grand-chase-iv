using System;
using System.Threading;
using Networking;

namespace CenterServer.Utilities
{
    public static class TitleTicker
    {
        private static Timer _timer;

        public static void Build()
        {
            if (_timer == null)
                _timer = new Timer(state => { Console.Title = $"Center Server | Sessions: {NetworkManager.Count}"; },
                    null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        public static void Dispose()
        {
            _timer?.Dispose();
            _timer = null;
        }
    }
}