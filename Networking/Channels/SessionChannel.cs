using System;
using System.Net.Sockets;
using System.Security.Cryptography;
using Commons.Packet;
using Commons.Utilities;
using Networking.Crypto;
using Networking.Packet;

namespace Networking.Channels
{
    public class SessionChannel
    {

        private readonly Socket _socket;
        
        public PlayerChannel PlayerChannel { get; private set; }
        
        public string Address => _socket.RemoteEndPoint.ToString().Split(':')[0];
        
        public readonly Cryptography Cryptography = new Cryptography();

        public SessionChannel(Socket socket)
        {
            _socket = socket;
        }

        public void Set(PlayerChannel playerChannel)
        {
            PlayerChannel = playerChannel;
        }
        
        public void Send(PacketSend packetSend, bool flag = false, short prefix = -1)
        {
            Send(packetSend.Write, packetSend.Code, flag, prefix);
        }
        
        public void Send(PacketWrite packetWrite, int code, bool flag = false, short prefix = -1)
        {
            var payload = new Payload(packetWrite.Packet, (short) code, flag);
            const int count = 1;
            var generateIv = new byte[8];
            var random = new Random();
            var byteTemp = (byte) random.Next(0x00, 0xFF);
            for (var i = 0; i < generateIv.Length; i++) generateIv[i] = byteTemp;
            var encryptBuffer = Cryptography.Encrypt(payload.Data, generateIv);
            var newBufferSize = (ushort) (16 + encryptBuffer.Length + 10);
            var newHmac = new HMACMD5(Cryptography.AuthenticKey);
            var concatBuffer = Sequence.Concat(BitConverter.GetBytes(prefix), BitConverter.GetBytes(count),
                generateIv, encryptBuffer);
            var authCode =
                Sequence.ReadBlock(
                    newHmac.ComputeHash(Sequence.Concat(BitConverter.GetBytes(prefix), BitConverter.GetBytes(count),
                        generateIv, encryptBuffer)), 0, 10);
            var bufferResult = Sequence.Concat(BitConverter.GetBytes(newBufferSize), concatBuffer, authCode);
            Console.WriteLine($"send: {code}:{bufferResult.Length}");
            NetworkServer.SendData(_socket, bufferResult);
        }

        public void Shutdown()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        public bool ReceiveAsync(SocketAsyncEventArgs receiveEventArgs)
        {
            return _socket.ReceiveAsync(receiveEventArgs);
        }
    }

}