using Commons.Database.Data.Users;

namespace Networking.Channels
{
    public abstract class PlayerChannel
    {
        public string Username { get; set; }

        public UsersModel UsersModel { get; set; }
        
        public int CurrentCharacter { get; set; }

        public int CharType { get; set; } = 0;

        public virtual bool EnterChannel(int channelId)
        {
            return false;
        }

        public virtual void SetRoom(object room) { }

        public virtual void LeaveChannel() { }

    }
}