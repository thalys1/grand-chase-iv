using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Commons.Log;
using Commons.Packet;
using Commons.Settings.Network;
using Networking.Channels;
using Networking.Packet;
using Networking.Pool;
using Networking.Token;

namespace Networking
{
    public static class NetworkServer
    {
        private static NetworkSettings _networkSettings;
        
        private static SemaphoreSlim _maxConnectionsEnforcer;
        private static SemaphoreSlim _maxSaeaSendEnforcer;
        private static SemaphoreSlim _maxAcceptOpsEnforcer;
        
        private static Socket _listenSocket;

        private static IPEndPoint _endPoint;

        private static INetworkHandler _networkHandler;
        
        private static NetworkAcceptPool _poolOfAcceptEventArgs;
        private static NetworkReceivePool _poolOfRecEventArgs;
        private static NetworkSendPool _poolOfSendEventArgs;

        public static void Configure(INetworkHandler networkHandler, NetworkSettings networkSettings)
        {
            _networkHandler = networkHandler;
            _networkSettings = networkSettings;
            _endPoint = new IPEndPoint(
                _networkSettings.Host.Equals("*")
                    ? IPAddress.Any
                    : (IPAddress.TryParse(_networkSettings.Host, out var address) ? address : IPAddress.Any),
                _networkSettings.Port);
            _poolOfAcceptEventArgs = new NetworkAcceptPool(_networkSettings.MaxSimultaneousAcceptOps, AcceptEventArg_Completed);
            _poolOfRecEventArgs = new NetworkReceivePool(_networkSettings.NumOfSaeaForRec, IO_ReceiveCompleted);
            _poolOfSendEventArgs = new NetworkSendPool(_networkSettings.NumOfSaeaForSend, IO_SendCompleted);

            _maxConnectionsEnforcer = new SemaphoreSlim(_networkSettings.MaxConnections, _networkSettings.MaxConnections);
            _maxSaeaSendEnforcer = new SemaphoreSlim(_networkSettings.NumOfSaeaForSend, _networkSettings.NumOfSaeaForSend);
            _maxAcceptOpsEnforcer = new SemaphoreSlim(_networkSettings.MaxSimultaneousAcceptOps, _networkSettings.MaxSimultaneousAcceptOps);
            
            _listenSocket = new Socket((_endPoint).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public static void Start()
        {
            _listenSocket.Bind(_endPoint);
            _listenSocket.Listen(_networkSettings.Backlog);
            StartAccept();
        }
        
        private static void StartAccept()
        {
            _maxAcceptOpsEnforcer.Wait();
            if (!_poolOfAcceptEventArgs.TryPop(out var acceptEventArgs)) return;
            try
            {
                _maxConnectionsEnforcer.Wait();
                var willRaiseEvent = _listenSocket.AcceptAsync(acceptEventArgs);
                if (!willRaiseEvent)
                {
                    ProcessAccept(acceptEventArgs);
                }
            }
            catch
            {
                // ignored
            }
        }
        
        private static void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private static void ProcessAccept(SocketAsyncEventArgs acceptEventArgs)
        {
            StartAccept();

            if (acceptEventArgs.SocketError != SocketError.Success)
            {
                HandleBadAccept(acceptEventArgs);
                _maxAcceptOpsEnforcer.Release();
                return;
            }
            if (_poolOfRecEventArgs.TryPop(out var recEventArgs))
            {
                var channel = new SessionChannel(acceptEventArgs.AcceptSocket);
                recEventArgs.UserToken = channel;
                
                acceptEventArgs.AcceptSocket = null;
                _poolOfAcceptEventArgs.Push(acceptEventArgs);
                _maxAcceptOpsEnforcer.Release();
                _networkHandler.OnChannelConnected(channel);
                StartReceive(recEventArgs);
            }
            else
            {
                HandleBadAccept(acceptEventArgs);
                WriteConsole.Warning("Cannot handle this session, there are no more receive objects available for us.");
            }
        }

        private static void IO_SendCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.LastOperation != SocketAsyncOperation.Send)
            {
                throw new InvalidOperationException("Tried to pass a send operation but the operation expected was not a send.");
            }

            ProcessSend(e);
        }

        private static void IO_ReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.LastOperation != SocketAsyncOperation.Receive)
            {
                throw new InvalidOperationException("Tried to pass a receive operation but the operation expected was not a receive.");
            }

            ProcessReceive(e);
        }

        private static void StartReceive(SocketAsyncEventArgs receiveEventArgs)
        {
            var channel = receiveEventArgs.UserToken as SessionChannel;
            try
            {
                var willRaiseEvent = channel.ReceiveAsync(receiveEventArgs);
                if (!willRaiseEvent)
                {
                    ProcessReceive(receiveEventArgs);
                }
            }
            catch (Exception exception)
            {
                _networkHandler.OnChannelException(exception, channel);
            }
        }

        private static void ProcessReceive(SocketAsyncEventArgs receiveEventArgs)
        {
            var channel = receiveEventArgs.UserToken as SessionChannel;
            if (receiveEventArgs.BytesTransferred > 32 && receiveEventArgs.SocketError == SocketError.Success)
            {
                var dataReceived = new byte[receiveEventArgs.BytesTransferred];
                Buffer.BlockCopy(receiveEventArgs.Buffer, receiveEventArgs.Offset, dataReceived, 0, receiveEventArgs.BytesTransferred);

                try
                {
                    var position = 0;
                    var packetRead = new PacketRead(dataReceived, 0);
                    var shortSize = packetRead.UShort();
                    while (position < dataReceived.Length)
                    {
                        if (position >= dataReceived.Length)
                            return;
                        var _buffer = new byte[shortSize];
                        Array.Copy(dataReceived, position, _buffer, 0, shortSize);
                        var newPacketRead = new PacketRead(_buffer, 0);
                        position += (ushort) ((dataReceived[position + 1] << 8) | dataReceived[position]);
                        var newSize = newPacketRead.UShort();
                        newPacketRead.Short();
                        newPacketRead.Int();
                        var iv = newPacketRead.Buffer_Array_Bytes(8);
                        var content = newPacketRead.Buffer_Array_Bytes(newSize - 16 - 10);
                        var getPayload = channel.Cryptography.Decrypt(content, iv);
                        var payloadPacketRead = new PacketRead(getPayload, 0, true);
                        var packetId = (ushort) ((getPayload[0] << 8) | getPayload[1]);
                        _networkHandler.OnChannelDataReceived(packetId, payloadPacketRead, channel);
                    }
                }
                catch (Exception exception)
                {
                    _networkHandler.OnChannelException(exception, channel);
                }

                StartReceive(receiveEventArgs);
            }
            else
            {
                CloseClientSocket(receiveEventArgs);
                ReturnReceiveSaea(receiveEventArgs);
            }
        }

        public static void SendData(Socket socket, byte[] data)
        {
            _maxSaeaSendEnforcer.Wait();
            if (!_poolOfSendEventArgs.TryPop(out var sendEventArgs)) return;
            var token = (SendDataToken) sendEventArgs.UserToken;
            token.DataToSend = data;
            token.SendBytesRemainingCount = data.Length;

            sendEventArgs.AcceptSocket = socket;
            StartSend(sendEventArgs);
        }

        private static void StartSend(SocketAsyncEventArgs sendEventArgs)
        {
            var token = (SendDataToken)sendEventArgs.UserToken;

            Console.WriteLine($"{token.SendBytesRemainingCount} : {_networkSettings.BufferSize}");
            if (token.SendBytesRemainingCount <= _networkSettings.BufferSize)
            {
                sendEventArgs.SetBuffer(sendEventArgs.Offset, token.SendBytesRemainingCount);
                Buffer.BlockCopy(token.DataToSend, token.BytesSentAlreadyCount, sendEventArgs.Buffer, sendEventArgs.Offset, token.SendBytesRemainingCount);
            }
            else
            {
                sendEventArgs.SetBuffer(sendEventArgs.Offset, _networkSettings.BufferSize);
                Buffer.BlockCopy(token.DataToSend, token.BytesSentAlreadyCount, sendEventArgs.Buffer, sendEventArgs.Offset, _networkSettings.BufferSize);
            }

            var willRaiseEvent = sendEventArgs.AcceptSocket.SendAsync(sendEventArgs);

            if (!willRaiseEvent)
            {
                ProcessSend(sendEventArgs);
            }
        }

        private static void ProcessSend(SocketAsyncEventArgs sendEventArgs)
        {
            var token = (SendDataToken) sendEventArgs.UserToken;

            if (sendEventArgs.SocketError == SocketError.Success)
            {
                token.SendBytesRemainingCount = token.SendBytesRemainingCount - sendEventArgs.BytesTransferred;

                if (token.SendBytesRemainingCount == 0)
                {
                    token.Reset();
                    ReturnSendSaea(sendEventArgs);
                }
                else
                {
                    token.BytesSentAlreadyCount += sendEventArgs.BytesTransferred;
                    StartSend(sendEventArgs);
                }
            }
            else
            {
                token.Reset();
                CloseClientSocket(sendEventArgs);
                ReturnSendSaea(sendEventArgs);
            }
        }

        private static void CloseClientSocket(SocketAsyncEventArgs args)
        {
            var channel = args.UserToken as SessionChannel;
            _networkHandler.OnChannelDisconnected(channel);
            try
            {
                channel.Shutdown();
            }
            catch (Exception exception)
            {
                // ignored
            }
        }

        private static void ReturnReceiveSaea(SocketAsyncEventArgs args)
        {
            _maxConnectionsEnforcer.Release();
            _poolOfRecEventArgs.Push(args);
        }

        private static void ReturnSendSaea(SocketAsyncEventArgs args)
        {
            _poolOfSendEventArgs.Push(args);
            _maxSaeaSendEnforcer.Release();
        }

        private static void HandleBadAccept(SocketAsyncEventArgs acceptEventArgs)
        {
            _poolOfAcceptEventArgs.Push(acceptEventArgs);
            try
            {
                acceptEventArgs.AcceptSocket.Shutdown(SocketShutdown.Both);
                acceptEventArgs.AcceptSocket.Close();
            }
            catch
            {
                
            }
        }

        [Obsolete]
        public static void Shutdown()
        {
            _listenSocket.Shutdown(SocketShutdown.Both);
            _listenSocket.Close();

            DisposeAllSaeaObjects();
        }

        private static void DisposeAllSaeaObjects()
        {
            _poolOfAcceptEventArgs.Dispose();
            _poolOfSendEventArgs.Dispose();
            _poolOfRecEventArgs.Dispose();
        }
    }

}