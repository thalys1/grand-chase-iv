using System;
using Commons.Packet;
using Networking.Channels;

namespace Networking
{
    public interface INetworkHandler
    {
        void OnChannelConnected(SessionChannel sessionChannel);
        
        void OnChannelDisconnected(SessionChannel sessionChannel);
        
        void OnChannelDataReceived(int packetId, PacketRead packetRead, SessionChannel sessionChannel);

        void OnChannelException(Exception exception, SessionChannel sessionChannel);
    }
}