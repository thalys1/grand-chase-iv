using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.ServiceModel.Channels;
using Networking.Token;

namespace Networking.Pool
{
    internal sealed class NetworkSendPool
    {
        private readonly ConcurrentStack<SocketAsyncEventArgs> _pool;

        public NetworkSendPool(int capacity, EventHandler<SocketAsyncEventArgs> completed, int singleBufferMaxSize = 8 * 1024)
        {
            _pool = new ConcurrentStack<SocketAsyncEventArgs>();
            var bufferManager = BufferManager.CreateBufferManager(singleBufferMaxSize * capacity, singleBufferMaxSize);
            for (var i = 0; i < capacity; i++)
            {
                var buffer = bufferManager.TakeBuffer(singleBufferMaxSize);
                var acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed += completed;
                acceptEventArg.UserToken = new SendDataToken();
                acceptEventArg.SetBuffer(buffer, 0, buffer.Length);
                _pool.Push(acceptEventArg);
            }
        }

        public bool TryPop(out SocketAsyncEventArgs args)
        {
            return _pool.TryPop(out args);
        }

        public void Push(SocketAsyncEventArgs args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("Items added to a SocketAsyncEventArgsPool cannot be null");
            }
            lock (this)
            {
                args.AcceptSocket = null;
                args.RemoteEndPoint = null;
                args.DisconnectReuseSocket = true;
            }
            _pool.Push(args);
        }

        public void Dispose()
        {
            while (_pool.Count > 0)
            {
                if (_pool.TryPop(out var eventArgs))
                {
                    eventArgs.Dispose();
                }
            }
        }
    }

}