using System.Collections.Concurrent;
using System.Reflection;
using Commons.Attributes;
using Commons.Log;

namespace Networking.Packet
{
    public abstract class PacketHandler
    {
        private readonly ConcurrentDictionary<int, IPacketRequest> _requests = new ConcurrentDictionary<int, IPacketRequest>();

        protected void Register(IPacketRequest packetRequest)
        {
            var type = packetRequest.GetType();
            try
            {
                var code = type.GetCustomAttribute<CodeInAttribute>().Code;
                if (_requests.ContainsKey(code))
                {
                    WriteConsole.Warning($"The packet [{packetRequest}] already exists in the registry.");
                }
                else
                {
                    if (!_requests.TryAdd(code, packetRequest))
                    {
                        WriteConsole.Warning($"Can't register packet [{packetRequest}].");
                    }
                }
            }
            catch
            {
                WriteConsole.Warning($"Can't register packet [{packetRequest}] or without attributes.");
            }
        }
        
        public bool TryGetValue(int code, out IPacketRequest packetRequest)
        {
            return _requests.TryGetValue(code, out packetRequest);
        }
    }
}