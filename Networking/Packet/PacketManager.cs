﻿namespace Networking.Packet
{
    public static class PacketManager
    {

        private static PacketHandler _packetHandler;

        public static void Configure(PacketHandler packetHandler)
        {
            _packetHandler = packetHandler;
        }

        public static bool TryRequest(int code, out IPacketRequest packetRequest)
        {
            return _packetHandler.TryGetValue(code, out packetRequest);
        }
    }
}