﻿using Commons.Packet;
using Networking.Channels;

namespace Networking.Packet
{
    public interface IPacketRequest
    {
        void Dispatch(SessionChannel session, PacketRead packetRead);
    }
}