using System.Collections.Generic;
using System.Net.Sockets;
using Networking.Channels;

namespace Networking
{
    public static class NetworkManager
    {
        private static readonly IList<SessionChannel> Connections = new List<SessionChannel>();

        public static int Count => Connections.Count;

        public static void Add(SessionChannel sessionChannel)
        {
            if (!Connections.Contains(sessionChannel)) Connections.Add(sessionChannel);
        }

        public static void Remove(SessionChannel sessionChannel)
        {
            if (Connections.Contains(sessionChannel)) Connections.Remove(sessionChannel);
        }
    }
}