﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Commons.Database;
using Commons.Database.Data.Servers;
using Commons.Log;
using Commons.Settings;
using Commons.Settings.Game;
using GameServer.Communication.Handler;
using GameServer.Game;
using GameServer.Game.Channels;
using GameServer.Utilities;
using Networking;
using Networking.Packet;

namespace GameServer
{
    public static class Emulator
    {
        private static readonly XmlSerializer<GameSettings> XmlSerializer = new XmlSerializer<GameSettings>();

        public static ServerModel ServerModel { get; private set; }

        public static GameSettings Settings => XmlSerializer.Data;

        public static void Main(string[] args)
        {
            Console.Title = "...";
            WriteConsole.Info("Loading server...");
            if (XmlSerializer.Load(Path.Combine(Application.StartupPath, @"GameServer.xml")))
            {
                Console.Title = $"[{XmlSerializer.Data.ServerId}] Loading game server...";
                if (DatabaseFactory.Build(XmlSerializer.Data.Database))
                {
                    PacketManager.Configure(new RequestHandler());
                    using (var openSession = DatabaseFactory.Session.OpenSession())
                    {
                        ServerModel = openSession.QueryOver<ServerModel>().Where(x => x.Id == XmlSerializer.Data.ServerId)
                            .SingleOrDefault();
                    }

                    foreach (var channel in XmlSerializer.Data.Channels)
                        ChannelManager.Add(new Channel
                            {Id = channel.ChannelId, Name = "", Limit = channel.Limit, Level = channel.Level});
                    NetworkServer.Configure(new ChannelHandler(), XmlSerializer.Data.Network);
                    WriteConsole.Info($"Server started on {XmlSerializer.Data.Network.Host}:{XmlSerializer.Data.Network.Port}");
                    TitleTicker.Build();
                    
                    NetworkServer.Start();
                }
            }

            while (true) Thread.Sleep(50);
        }
    }
}