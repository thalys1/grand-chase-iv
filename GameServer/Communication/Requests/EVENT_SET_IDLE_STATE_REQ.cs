﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_SET_IDLE_STATE_REQ)]
    public class EVENT_SET_IDLE_STATE_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 05 A3 BD 00 00 00 00 00");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_IDLE_STATE_NOT);
            }
        }
    }
}