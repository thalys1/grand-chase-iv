﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_AGIT_MAP_CATALOGUE_REQ)]
    public class EVENT_AGIT_MAP_CATALOGUE_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray(
                    "00 09 3C 56 00 00 00 04 00 00 00 00 00 00 00 00 00 00 00 3C 00 02 00 02 00 00 00 0F 4D 79 46 69 72 73 74 41 7A 69 74 2E 6C 75 61 00 00 00 01 00 00 00 01 00 00 03 84 00 02 00 02 00 00 00 12 41 7A 69 74 32 30 31 5F 39 30 62 79 36 30 2E 6C 75 61 00 00 00 02 00 00 00 02 00 00 0A F0 00 03 00 03 00 00 00 12 41 7A 69 74 33 30 31 5F 39 30 62 79 36 30 2E 6C 75 61 00 00 00 03 00 00 00 03 00 00 0C E4 00 03 00 03 00 00 00 12 41 7A 69 74 33 30 32 5F 39 30 62 79 36 30 2E 6C 75 61 00 00 00 00");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_AGIT_MAP_CATALOGUE_ACK);
            }
        }
    }
}