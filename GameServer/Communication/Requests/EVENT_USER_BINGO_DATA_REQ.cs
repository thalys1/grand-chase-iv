﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_USER_BINGO_DATA_REQ)]
    public class EVENT_USER_BINGO_DATA_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray(
                    "00 00 00 01 02 71 63 C0 72 93 2F 20 00 00 00 00 08 7C 0B 40 00 00 00 00 00 00 00 00 01 FF 02 5B 7E 97 FF FF FF 63 02 55 AC");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_USER_BINGO_DATA_ACK);
            }
        }
    }
}