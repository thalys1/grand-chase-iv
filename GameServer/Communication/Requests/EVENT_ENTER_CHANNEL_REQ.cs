﻿using Commons.Attributes;
using Commons.Packet;
using GameServer.Game;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_ENTER_CHANNEL_REQ)]
    public class EVENT_ENTER_CHANNEL_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            sessionChannel.PlayerChannel.LeaveChannel();
            var channelId = packetRead.Int();
            if (!sessionChannel.PlayerChannel.EnterChannel(channelId)) return;
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 00 00 00 00 59 23 DD F0 59 25 2F 6F");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_ENTER_CHANNEL_ACK);
            }
        }
    }
}