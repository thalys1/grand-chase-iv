﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_INVEN_BUFF_ITEM_LIST_REQ)]
    public class EVENT_INVEN_BUFF_ITEM_LIST_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray(
                    "00 00 00 03 00 08 87 0C 00 00 01 54 00 00 00 00 00 02 00 00 00 00 01 00 00 00 02 00 08 87 16 00 00 01 55 00 00 00 00 00 01 00 00 00 00 01 00 00 00 02 00 08 87 20 00 00 01 56 00 00 00 00 00 00 00 00 00 00 01 00 00 00 02");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_INVEN_BUFF_ITEM_LIST_ACK);
            }
        }
    }
}