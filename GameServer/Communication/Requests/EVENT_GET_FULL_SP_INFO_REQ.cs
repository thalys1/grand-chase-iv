﻿using System.Linq;
using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_GET_FULL_SP_INFO_REQ)]
    public class EVENT_GET_FULL_SP_INFO_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            var promotions = sessionChannel.PlayerChannel.UsersModel.Characters.Sum(x => x.Promotion);

            using (var packetWrite = new PacketWrite())
            {
                packetWrite.Int(promotions);

                foreach (var character in sessionChannel.PlayerChannel.UsersModel.Characters)
                {
                    packetWrite.Byte((byte) character.CharacterType);
                    packetWrite.Byte(255);
                    packetWrite.Byte((byte) character.CharacterType);
                    packetWrite.Byte(255);
                    packetWrite.Byte((byte) character.Skills.Count);
                    foreach (var skill in character.Skills) packetWrite.Int(skill.SkillId);

                    for (var promotion = 0; promotion <= character.Promotion; promotion++)
                    {
                        var st = character.Skills.Count(t => t.Promotion == promotion);
                        packetWrite.Byte((byte) character.CharacterType);
                        packetWrite.Byte((byte) promotion);
                        packetWrite.Byte((byte) character.CharacterType);
                        packetWrite.Byte((byte) promotion);
                        packetWrite.Int(st);
                        foreach (var skill in character.Skills)
                            if (skill.Promotion == promotion)
                                packetWrite.Int(skill.SkillId);
                    }
                }

                var stChars = sessionChannel.PlayerChannel.UsersModel.Characters.Sum(x => x.Promotion);
                packetWrite.Int(stChars);

                foreach (var character in sessionChannel.PlayerChannel.UsersModel.Characters)
                    for (byte y = 0; y <= character.Promotion; y++)
                    {
                        packetWrite.Byte((byte) character.CharacterType);
                        packetWrite.Byte(y);
                        packetWrite.Int(2);
                        packetWrite.Int(0);
                        packetWrite.Int(character.Skills.Count);
                        foreach (var skill in character.Skills)
                        {
                            packetWrite.Int(0);
                            packetWrite.Int(skill.SetId);
                            packetWrite.Int(skill.SkillId);
                        }

                        packetWrite.Int(1);
                        packetWrite.Int(character.Skills.Count);
                        foreach (var skill in character.Skills)
                        {
                            packetWrite.Int(0);
                            packetWrite.Int(skill.SetId);
                            packetWrite.Int(skill.SkillId);
                        }
                    }

                packetWrite.Int(stChars);
                foreach (var character in sessionChannel.PlayerChannel.UsersModel.Characters)
                    for (var promotion = 0; promotion <= character.Promotion; promotion++)
                    {
                        packetWrite.Byte((byte) character.CharacterType);
                        packetWrite.Byte((byte) promotion);
                        packetWrite.Int(0);
                    }

                packetWrite.HexArray(
                    "00 00 00 50 00 00 00 00 00 00 00 01 00 00 00 00 00 02 00 00 00 00 00 03 00 00 00 00 01 00 00 00 00 00 01 01 00 00 00 00 01 02 00 00 00 00 01 03 00 00 00 00 02 00 00 00 00 00 02 01 00 00 00 00 02 02 00 00 00 00 02 03 00 00 00 00 03 00 00 00 00 00 03 01 00 00 00 00 03 02 00 00 00 00 03 03 00 00 00 00 04 00 00 00 00 00 04 01 00 00 00 00 04 02 00 00 00 00 04 03 00 00 00 00 05 00 00 00 00 00 05 01 00 00 00 00 05 02 00 00 00 00 05 03 00 00 00 00 06 00 00 00 00 00 06 01 00 00 00 00 06 02 00 00 00 00 06 03 00 00 00 00 07 00 00 00 00 00 07 01 00 00 00 00 07 02 00 00 00 00 07 03 00 00 00 00 08 00 00 00 00 00 08 01 00 00 00 00 08 02 00 00 00 00 08 03 00 00 00 00 09 00 00 00 00 00 09 01 00 00 00 00 09 02 00 00 00 00 09 03 00 00 00 00 0A 00 00 00 00 00 0A 01 00 00 00 00 0A 02 00 00 00 00 0A 03 00 00 00 00 0B 00 00 00 00 00 0B 01 00 00 00 00 0B 02 00 00 00 00 0B 03 00 00 00 00 0C 00 00 00 00 00 0C 01 00 00 00 00 0C 02 00 00 00 00 0C 03 00 00 00 00 0D 00 00 00 00 00 0D 01 00 00 00 00 0D 02 00 00 00 00 0D 03 00 00 00 00 0E 00 00 00 00 00 0E 01 00 00 00 00 0E 02 00 00 00 00 0E 03 00 00 00 00 0F 00 00 00 00 00 0F 01 00 00 00 00 0F 02 00 00 00 00 0F 03 00 00 00 00 10 00 00 00 00 00 10 01 00 00 00 00 10 02 00 00 00 00 10 03 00 00 00 00 11 00 00 00 00 00 11 01 00 00 00 00 11 02 00 00 00 00 11 03 00 00 00 00 12 00 00 00 00 00 12 01 00 00 00 00 12 02 00 00 00 00 12 03 00 00 00 00 13 00 00 00 00 00 13 01 00 00 00 00 13 02 00 00 00 00 13 03 00 00 00 00 00 00 00 00");
                packetWrite.HexArray(
                    "00 00 00 14 00 00 00 00 00 01 00 00 00 00 02 00 00 00 00 03 00 00 00 00 04 00 00 00 00 05 00 00 00 00 06 00 00 00 00 07 00 00 00 00 08 00 00 00 00 09 00 00 00 00 0A 00 00 00 00 0B 00 00 00 00 0C 00 00 00 00 0D 00 00 00 00 0E 00 00 00 00 0F 00 00 00 00 10 00 00 00 00 11 00 00 00 00 12 00 00 00 00 13 00 00 00 00");
                packetWrite.Int(0);

                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_GET_FULL_SP_INFO_ACK);
            }
        }
    }
}