﻿using Commons.Attributes;
using Commons.Database;
using Commons.Database.Data.PetsItem;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_PET_COSTUM_LIST_REQ)]
    public class EVENT_PET_COSTUM_LIST_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {

                using (var openSession = DatabaseFactory.Session.OpenSession())
                {
                    var pets = openSession.QueryOver<PetsModel>().List();
                    packetWrite.Int(pets.Count);
                    foreach (var pet in pets)
                    {
                        packetWrite.Short((short) pet.ItemId);
                        packetWrite.Byte((byte) pet.Promotion);
                        packetWrite.Int(1);
                        packetWrite.Short((short) pet.PetId);
                    }

                    packetWrite.Byte(0);
                }

                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_PET_COSTUM_LIST_ACK);
            }
        }
    }
}