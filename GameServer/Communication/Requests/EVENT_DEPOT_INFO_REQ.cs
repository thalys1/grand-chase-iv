﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_DEPOT_INFO_REQ)]
    public class EVENT_DEPOT_INFO_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 03 00 00 00 05 00 0C 3A FA 00 0C 3A F0 00");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_DEPOT_INFO_ACK);
            }
        }
    }
}