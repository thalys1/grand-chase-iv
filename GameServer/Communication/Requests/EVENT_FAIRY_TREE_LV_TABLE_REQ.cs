﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_FAIRY_TREE_LV_TABLE_REQ)]
    public class EVENT_FAIRY_TREE_LV_TABLE_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray(
                    "00 00 00 07 00 00 00 01 00 00 00 00 00 00 00 02 00 00 00 0A 00 00 00 03 00 00 00 1E 00 00 00 04 00 00 00 3C 00 00 00 05 00 00 00 64 00 00 00 06 00 00 01 2C 00 00 00 07 00 00 03 84 00 00 00 00 01");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_FAIRY_TREE_LV_TABLE_ACK);
            }
        }
    }
}