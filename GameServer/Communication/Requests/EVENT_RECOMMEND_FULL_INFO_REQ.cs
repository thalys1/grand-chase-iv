﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_RECOMMEND_FULL_INFO_REQ)]
    public class EVENT_RECOMMEND_FULL_INFO_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray(
                    "00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_RECOMMEND_FULL_INFO_ACK);
            }
        }
    }
}