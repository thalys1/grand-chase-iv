﻿using Commons.Attributes;
using Commons.Packet;
using GameServer.Game.Players;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_ROOM_LIST_REQ)]
    public class EVENT_ROOM_LIST_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            var player = (Player) sessionChannel.PlayerChannel;
            if (sessionChannel == null) return;
            var roomType = packetRead.Byte();
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.Int(player.CurrentChannel.Rooms); //packetWrite.Int(player.Rooms);
                //foreach (var room in channel.Rooms)
                //{
                //   packetWrite.Byte(0);
                //    packetWrite.Short((short) room.Id);
                //    packetWrite.UnicodeStr(room.Name);
                //    packetWrite.Byte(room.Password.Length > 0 ? (byte) 0 : (byte) 1);
                //    packetWrite.Byte(0);
                //    packetWrite.UnicodeStr(room.Password);
                //    packetWrite.Short((short) (room.Sessions.Count + room.SlotsOpen));
                //    packetWrite.Short((short) room.Sessions.Count);
                //    packetWrite.Bool(room.Playing);
                //    packetWrite.HexArray("2E 02 1B 25 01 00 00 00 00 01 6B F9 38 77 00 00 00 0C 00 00 00 00 00 00 00 01");
                //    packetWrite.UnicodeStr(room.Leader.UserData.Username);
                //    packetWrite.HexArray("0B 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 01");

                //}
                packetWrite.Int(0);
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_ROOM_LIST_ACK);
            }
        }
    }
}