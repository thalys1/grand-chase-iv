﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.HEART_BIT_NOT)]
    public class HEART_BIT_NOT : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.Int(0);
                sessionChannel.Send(packetWrite, (short) Opcodes.HEART_BIT_NOT);
            }
        }
    }
}