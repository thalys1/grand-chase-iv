﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_COST_RATE_FOR_GAMBLE_BUY_REQ)]
    public class EVENT_COST_RATE_FOR_GAMBLE_BUY_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("40 00 00 00 00 00 00 00 01");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_COST_RATE_FOR_GAMBLE_BUY_ACK);
            }
        }
    }
}