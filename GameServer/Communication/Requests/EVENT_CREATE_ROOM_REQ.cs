﻿using System;
using Commons.Attributes;
using Commons.Database;
using Commons.Database.Data.UsersCharacters;
using Commons.Database.Data.UsersCharactersEquipments;
using Commons.Packet;
using Commons.Serializables.Stages;
using Commons.Serializables.Users;
using GameServer.Communication.Serializables.Rooms;
using GameServer.Game.Channels.Rooms;
using GameServer.Game.Channels.Rooms.Slots;
using GameServer.Game.Players;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_CREATE_ROOM_REQ)]
    public class EVENT_CREATE_ROOM_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                var player = (Player) sessionChannel.PlayerChannel;
                Rooms.SerializeRoomInfo(player, packetRead);
                var reliant = packetRead.Buffer_Array_Bytes(78);
                Users.SerializeUserInfo(player, packetRead);
                packetWrite.Int(0);
                Users.SerializeUserInfo(player, packetWrite);
                Stages.Serialize(packetWrite, player.UsersModel.Stages);
                packetWrite.HexArray("01 01 00 00 00 00 00 00 00 00 00 00 00 00");
                Users.SerializeCharacterInfo(player, packetWrite);
                packetWrite.HexArray("00 00 00 02 6B 00 A8 C0 E4 02 2D B1 00 00 00 01 7F 1A 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 E5 6A 00 00 00 00 00 D9 92 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 5A 00 5A 00 0B 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 01 00 00 00 01 00 00 00 00 00 00 00 D2 F0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 D2 F0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 01 00 00 00 00 85 89 82 21 00 00 00 00 00 00 00 00 01 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                Rooms.SerializeRoomInfo(player, packetWrite);
                packetWrite.ArrayBytes(reliant);
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_CREATE_ROOM_ACK);
            }
        }
    }
}