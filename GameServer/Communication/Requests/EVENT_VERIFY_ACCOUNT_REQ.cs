﻿using Commons.Attributes;
using Commons.Database;
using Commons.Database.Data.Users;
using Commons.Log;
using Commons.Packet;
using Commons.Serializables.Characters;
using Commons.Serializables.Quests;
using Commons.Serializables.Stages;
using GameServer.Communication.Send;
using GameServer.Game;
using GameServer.Game.Players;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_VERIFY_ACCOUNT_REQ)]
    public class EVENT_VERIFY_ACCOUNT_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            var username = packetRead.String();
            var password = packetRead.String();
            WriteConsole.Server($"VERIFY ACCOUNT: {username}:{password}");
            //try
            //{
            using (var openSession = DatabaseFactory.Session.OpenSession())
            {
                var userModel = openSession.QueryOver<UsersModel>().Where(x => x.Username == username)
                    .Where(x => x.Password == password).SingleOrDefault();
                if (userModel == null)
                {
                    using (var packetWrite = new PacketWrite())
                    {
                        packetWrite.Int(20);
                        packetWrite.UnicodeStr(username);
                        packetWrite.Int(0);
                        sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_VERIFY_ACCOUNT_ACK);
                    }
                    return;
                }

                if (userModel.Online == 1)
                {
                    using (var packetWrite = new PacketWrite())
                    {
                        WriteConsole.Server($"Attempted connection to a connected account: {userModel.Username}");
                        packetWrite.Int(1);
                        sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_USER_AUTH_CHECK_ACK);
                    }
                    return;
                }

                var player = new Player {Username = username, UsersModel = userModel};
                if (PlayersManager.TryAdd(player))
                {
                    sessionChannel.Set(player);
                    sessionChannel.Send(new EVENT_EXP_TABLE_NOT());
                    sessionChannel.Send(new EVENT_VERIFY_INVENTORY_NOT(userModel.Id));
                    using (var packetWrite = new PacketWrite())
                    {
                        packetWrite.UnicodeStr(username);
                        packetWrite.UnicodeStr(userModel.Username);
                        packetWrite.Byte(0);
                        packetWrite.Int(userModel.GamePoints);
                        packetWrite.HexArray("00 00 00 00 48 00 00 00");
                        packetWrite.Byte(userModel.Gender);
                        packetWrite.WriteIP(sessionChannel.Address);
                        packetWrite.Int(userModel.Id);
                        packetWrite.Int(0);
                        packetWrite.Int(0);
                        packetWrite.Int(userModel.GuildId); // Guild ID
                        packetWrite.Byte(255);
                        packetWrite.HexArray(
                            "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 64 00 00");
                        Characters.SerializeVerify(packetWrite, userModel.Characters);
                        packetWrite.Short((short) Emulator.ServerModel.RelayPort); // udp port
                        packetWrite.Int(userModel.Id);
                        packetWrite.UnicodeStr(Emulator.ServerModel.Name); // server name
                        packetWrite.Int(1); // m_iSessionInfo

                        packetWrite.Int(Emulator.ServerModel.Welcome.Length > 0 ? 1 : 0); // m_vecMsg
                        if (Emulator.ServerModel.Welcome.Length > 0)
                            packetWrite.UnicodeStr(Emulator.ServerModel.Welcome);

                        // stage
                        Stages.Serialize(packetWrite, userModel.Stages);
                        //
                        packetWrite.HexArray("13 0B 75 8D"); //m_nConnectType
                        // quests
                        Quests.Serialize(packetWrite, userModel.Quests);
                        //


                        packetWrite.Int(Emulator.ServerModel.Type);

                        packetWrite.HexArray("00 00 00 00 00 5A"); //m_iRP
                        packetWrite.HexArray("00 00 00 00"); //m_iRPRank
                        packetWrite.HexArray("00 80"); //m_dwAuthType
                        packetWrite.HexArray("00 00 00 00 00 00 00 00");

                        packetWrite.UnicodeStr("MsgServer_05"); // msg
                        packetWrite.Str("127.0.0.1"); // MG IP
                        packetWrite.Short(9300); // MSG Port
                        packetWrite.Int(292);
                        packetWrite.Int(0);
                        packetWrite.Int(0);
                        packetWrite.Int(-1);
                        packetWrite.Int(-1);
                        packetWrite.Str("127.0.0.1"); // MSG IP
                        packetWrite.HexArray("00 00 00 00");
                        packetWrite.HexArray("00 00 00 00");
                        packetWrite.HexArray("03 57 F1 73 AC 57 F1 73 AC 00 00 00 00");
                        // pets
                        packetWrite.Int(0);
                        //
                        packetWrite.HexArray("00 00 00 00");
                        packetWrite.Bool(false);
                        packetWrite.Int(userModel.InventorySize);
                        packetWrite.Int(0);
                        packetWrite.Int(userModel.LifeBonus);
                        packetWrite.HexArray(
                            "00 00 00 00 00 01 00 00 00 01 61 D0 B2 C0 00 64 7E EE E2 C0 07 E7 10 6B 7C 92 A0 00 00 00 00 A4 72 93 E0 57 EF 5E F0 00 00 00 00");
                        packetWrite.Int(20);
                        for (var chars = 0; chars < 20; chars++)
                        {
                            packetWrite.Int(chars);
                            packetWrite.Int(chars);
                            packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00");
                        }

                        packetWrite.Int(0); // gacha sr count
                        packetWrite.Int(0); // gacha sr location
                        packetWrite.Short(0); // gacha pet

                        packetWrite.Int(400); //Unk
                        packetWrite.Byte(0); //player.Account.NewBie
                        sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_VERIFY_ACCOUNT_ACK);

                        sessionChannel.Send(new EVENT_SERVER_TIME_NOT());
                        sessionChannel.Send(new EVENT_PET_VESTED_ITEM_NOT());
                        sessionChannel.Send(new EVENT_GRADUATE_CHARACTER_USER_INFO_NOT());
                        sessionChannel.Send(new EVENT_JUMPING_CHAR_INFO_NOT());
                        sessionChannel.Send(new EVENT_SLOT_INFO_NOT());
                        sessionChannel.Send(new EVENT_SYSTEM_GUIDE_COMPLETE_INFO_NOT());
                        sessionChannel.Send(new EVENT_FULL_LOOK_INFO_NOT());
                        sessionChannel.Send(new EVENT_FAIRY_TREE_BUFF_NOT());
                        sessionChannel.Send(new EVENT_AGIT_INIT_SEED_NOT());
                    }
                }
                else
                {
                    using (var packetWrite = new PacketWrite())
                    {
                        packetWrite.Int(20);
                        packetWrite.UnicodeStr(username);
                        packetWrite.Int(0);
                        sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_VERIFY_ACCOUNT_ACK);
                    }
                }
            }

            //}
            //catch
            //{
            //    packetWrite.Int(20);
            //   packetWrite.UnicodeStr(username);
            ////   packetWrite.Int(0);
            //   session.SendPacket(packetWrite, Opcodes.EVENT_VERIFY_ACCOUNT_ACK);
            //}
        }
    }
}