﻿using Commons.Attributes;
using Commons.Packet;
using GameServer.Game;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_LEAVE_CHANNEL_NOT)]
    public class EVENT_LEAVE_CHANNEL_NOT : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            sessionChannel.PlayerChannel.LeaveChannel();
        }
    }
}