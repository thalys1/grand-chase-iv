using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_CHANGE_SLOT_EQUIP_REQ)]
    public class EVENT_CHANGE_SLOT_EQUIP_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var packetWrite = new PacketWrite())
            {
                packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 00");
                sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_CHANGE_SLOT_EQUIP_ACK);
            }
        }
    }
}