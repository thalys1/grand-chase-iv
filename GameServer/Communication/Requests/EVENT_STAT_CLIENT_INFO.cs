﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_STAT_CLIENT_INFO)]
    public class EVENT_STAT_CLIENT_INFO : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
        }
    }
}