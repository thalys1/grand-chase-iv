using Commons.Attributes;
using Commons.Database;
using Commons.Database.Data.Servers;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_SERVER_LIST_REQ)]
    public class EVENT_SERVER_LIST_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            using (var openSession = DatabaseFactory.Session.OpenSession())
            {
                var servers = openSession.QueryOver<ServerModel>().List();
                using (var packetWrite = new PacketWrite())
                {
                    packetWrite.Int(servers.Count);
                    foreach (var server in servers)
                    {
                        packetWrite.Int(server.Id);
                        packetWrite.Int(server.Id);
                        packetWrite.UnicodeStr(server.Name);
                        packetWrite.Str(server.Address);
                        packetWrite.Short((short) server.Port);
                        packetWrite.Int(server.Users);
                        packetWrite.Int(server.Limit);
                        packetWrite.Int(server.Flag);
                        packetWrite.Int(-1);
                        packetWrite.Int(-1);
                        packetWrite.Str(server.Address);
                        packetWrite.UnicodeStr(server.Description);
                        packetWrite.Int(server.Type);
                    }

                    sessionChannel.Send(packetWrite, (short) Opcodes.EVENT_SERVER_LIST_ACK);
                }
            }
        }
    }
}