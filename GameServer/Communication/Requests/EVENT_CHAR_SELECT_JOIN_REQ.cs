﻿using Commons.Attributes;
using Commons.Packet;
using GameServer.Communication.Send;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_CHAR_SELECT_JOIN_REQ)]
    public class EVENT_CHAR_SELECT_JOIN_REQ : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            sessionChannel.Send(new EVENT_NONE_INVEN_ITEM_LIST_NOT());
            sessionChannel.Send(new EVENT_STRENGTH_MATERIAL_INFO());
            sessionChannel.Send(new EVENT_WEEKLY_REWARD_LIST_NOT());
            sessionChannel.Send(new EVENT_MONTHLY_REWARD_LIST_NOT());
            sessionChannel.Send(new EVENT_MATCH_RANK_REWARD_NOT());
            sessionChannel.Send(new EVENT_HERO_DUNGEON_INFO_NOT());
            sessionChannel.Send(new EVENT_USER_CHANGE_WEAPON_NOT());
            sessionChannel.Send(new EVENT_NEW_CHAR_CARD_INFO_NOT());
            sessionChannel.Send(new EVENT_VIRTUAL_CASH_LIMIT_RATIO_NOT());
            sessionChannel.Send(new EVENT_BAD_USER_INFO_NOT());
            sessionChannel.Send(new EVENT_COLLECTION_MISSION_NOT());
            sessionChannel.Send(new EVENT_HELL_TICKET_FREE_MODE_NOT());
            sessionChannel.Send(new EVENT_VIP_ITEM_LIST_NOT());
            sessionChannel.Send(new EVENT_CAPSULE_LIST_NOT());
            sessionChannel.Send(new EVENT_MISSION_PACK_LIST_NOT());
            sessionChannel.Send(new EVENT_CURRENT_VIRTUAL_CASH_NOT(sessionChannel.PlayerChannel.UsersModel.VirtualCash));
        }
    }
}