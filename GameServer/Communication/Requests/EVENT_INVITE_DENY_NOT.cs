﻿using Commons.Attributes;
using Commons.Packet;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Requests
{
    [CodeIn(Opcodes.EVENT_INVITE_DENY_NOT)]
    public class EVENT_INVITE_DENY_NOT : IPacketRequest
    {
        public void Dispatch(SessionChannel sessionChannel, PacketRead packetRead)
        {
            var inviteDeny = packetRead.Bool();
        }
    }
}