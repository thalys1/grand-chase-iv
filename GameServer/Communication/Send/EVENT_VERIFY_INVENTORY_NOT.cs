﻿using Commons.Database;
using Commons.Database.Data.UsersInventory;
using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_VERIFY_INVENTORY_NOT : PacketSend
    {
        public EVENT_VERIFY_INVENTORY_NOT(int userId) : base((short) Opcodes.EVENT_VERIFY_INVENTORY_NOT)
        {
            using (var openSession = DatabaseFactory.Session.OpenSession())
            {
                using (var transaction = openSession.BeginTransaction())
                {
                    var inventoryItems = openSession.QueryOver<UsersInventoryModel>().Where(x => x.UserId == userId)
                        .List();
                    Write.Int(1);
                    Write.Int(1);
                    Write.Int(inventoryItems.Count);
                    foreach (var item in inventoryItems)
                    {
                        Write.Int(item.ItemId);
                        Write.Int(1);
                        Write.Int(item.ItemId);
                        Write.Int(item.Quantity);
                        Write.Int(-1);
                        Write.Int(0);
                        Write.Short(0);
                        Write.Int(-1);
                        Write.Int(0);
                        Write.Int(1450577322);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Int(0);
                        Write.Byte(0);
                    }

                    transaction.Commit();
                }
            }
        }
    }
}