﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_CURRENT_VIRTUAL_CASH_NOT : PacketSend
    {
        public EVENT_CURRENT_VIRTUAL_CASH_NOT(int virtualCash) : base((short) Opcodes.EVENT_CURRENT_VIRTUAL_CASH_NOT)
        {
            Write.Int(virtualCash);
        }
    }
}