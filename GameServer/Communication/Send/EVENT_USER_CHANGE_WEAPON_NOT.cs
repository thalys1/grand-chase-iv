﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_USER_CHANGE_WEAPON_NOT : PacketSend
    {
        public EVENT_USER_CHANGE_WEAPON_NOT() : base((short) Opcodes.EVENT_USER_CHANGE_WEAPON_NOT)
        {
            Write.HexArray(
                "00 00 00 03 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 00 00");
        }
    }
}