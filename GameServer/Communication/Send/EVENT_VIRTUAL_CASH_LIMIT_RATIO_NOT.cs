﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_VIRTUAL_CASH_LIMIT_RATIO_NOT : PacketSend
    {
        public EVENT_VIRTUAL_CASH_LIMIT_RATIO_NOT() : base((short) Opcodes.EVENT_VIRTUAL_CASH_LIMIT_RATIO_NOT)
        {
            Write.HexArray("00 00 00 00 00 00 00 00 01");
        }
    }
}