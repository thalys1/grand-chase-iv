﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_GRADUATE_CHARACTER_USER_INFO_NOT : PacketSend
    {
        public EVENT_GRADUATE_CHARACTER_USER_INFO_NOT() : base((short) Opcodes.EVENT_GRADUATE_CHARACTER_USER_INFO_NOT)
        {
            Write.HexArray(
                "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 11 00 00 00 12 00 00 00 00");
        }
    }
}