﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_MISSION_PACK_LIST_NOT : PacketSend
    {
        public EVENT_MISSION_PACK_LIST_NOT() : base((short) Opcodes.EVENT_MONTHLY_REWARD_LIST_NOT)
        {
            Write.HexArray("00 00 00 01 00 02 9B EE 00 00 00 02 00 02 9C 2A 00 02 9C 34");
        }
    }
}