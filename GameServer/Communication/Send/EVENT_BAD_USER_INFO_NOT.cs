﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_BAD_USER_INFO_NOT : PacketSend
    {
        public EVENT_BAD_USER_INFO_NOT() : base((short) Opcodes.EVENT_BAD_USER_INFO_NOT)
        {
            Write.HexArray("00 00 00 00 00 00 00 00 01");
        }
    }
}