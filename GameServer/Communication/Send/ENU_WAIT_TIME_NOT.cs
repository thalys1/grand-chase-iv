using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class ENU_WAIT_TIME_NOT : PacketSend
    {
        public ENU_WAIT_TIME_NOT(int time) : base((short) Opcodes.ENU_WAIT_TIME_NOT)
        {
            Write.Int(time);
        }
    }
}