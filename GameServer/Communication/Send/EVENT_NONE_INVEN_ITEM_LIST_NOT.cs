﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_NONE_INVEN_ITEM_LIST_NOT : PacketSend
    {
        public EVENT_NONE_INVEN_ITEM_LIST_NOT() : base((short) Opcodes.EVENT_NONE_INVEN_ITEM_LIST_NOT)
        {
            Write.Int(1);
            Write.Int(6280);

            Write.Int(2);
            Write.Int(6280);

            Write.Int(0);
            Write.Int(10000);
        }
    }
}