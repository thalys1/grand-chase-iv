﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_SLOT_INFO_NOT : PacketSend
    {
        public EVENT_SLOT_INFO_NOT() : base((short) Opcodes.EVENT_SLOT_INFO_NOT)
        {
            Write.HexArray(
                "00 00 00 02 00 00 00 00 00 00 E5 6A 00 00 00 01 31 7F 24 36 00 00 00 00 01 00 00 E5 88 00 00 00 01 31 7F 24 37 00 00 00 00");
        }
    }
}