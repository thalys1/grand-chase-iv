﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_SYSTEM_GUIDE_COMPLETE_INFO_NOT : PacketSend
    {
        public EVENT_SYSTEM_GUIDE_COMPLETE_INFO_NOT() : base((short) Opcodes.EVENT_SYSTEM_GUIDE_COMPLETE_INFO_NOT)
        {
            Write.HexArray("00 00 00 00 00 00 00 00 00");
        }
    }
}