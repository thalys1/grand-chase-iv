﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_PET_VESTED_ITEM_NOT : PacketSend
    {
        public EVENT_PET_VESTED_ITEM_NOT() : base((short) Opcodes.EVENT_PET_VESTED_ITEM_NOT)
        {
            Write.HexArray(
                "00 00 00 08 00 11 1D 90 00 13 AF EC 00 12 C8 FC 00 15 6D BE 00 15 6D DC 00 15 6D FA 00 15 6D 0A 00 13 AF A6");
        }
    }
}