﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_AGIT_INIT_SEED_NOT : PacketSend
    {
        public EVENT_AGIT_INIT_SEED_NOT() : base((short) Opcodes.EVENT_AGIT_INIT_SEED_NOT)
        {
            Write.HexArray(
                "00 05 A3 BD 57 F1 73 A5 00 00 00 14 00 00 00 01 00 09 3C 4C 00 00 00 01 31 7F 79 7D 00 00 00 14 00 00 00 14 00 00 00 00 00 00 FF FF FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 00 00 00 00 00 00 00 00");
        }
    }
}