﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_FAIRY_TREE_BUFF_NOT : PacketSend
    {
        public EVENT_FAIRY_TREE_BUFF_NOT() : base((short) Opcodes.EVENT_FAIRY_TREE_BUFF_NOT)
        {
            Write.HexArray("00 05 A3 BD 00 00 00 01 00 00 00 00 00 00 00 00 57 F1 73 A5 00 00 00 00 01");
        }
    }
}