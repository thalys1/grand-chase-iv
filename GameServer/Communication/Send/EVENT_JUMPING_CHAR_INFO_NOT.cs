﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_JUMPING_CHAR_INFO_NOT : PacketSend
    {
        public EVENT_JUMPING_CHAR_INFO_NOT() : base((short) Opcodes.EVENT_JUMPING_CHAR_INFO_NOT)
        {
            Write.HexArray("00 00 00 00 46 00 00 00 11 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F");
        }
    }
}