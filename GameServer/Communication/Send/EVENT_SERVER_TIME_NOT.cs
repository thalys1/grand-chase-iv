﻿using System;
using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_SERVER_TIME_NOT : PacketSend
    {
        public EVENT_SERVER_TIME_NOT() : base((short) Opcodes.EVENT_SERVER_TIME_NOT)
        {
            var d1 = DateTime.Parse("1970-01-01 09:00:00");
            var d2 = DateTime.Now;
            var d3 = d2 - d1;
            Write.Int(Convert.ToInt32(d3.TotalSeconds));
            Write.Int(d2.Year);
            Write.Int(d2.Month);
            Write.Int(d2.Day);
            Write.Int(d2.Hour);
            Write.Int(d2.Minute);
            Write.Int(d2.Second);
        }
    }
}