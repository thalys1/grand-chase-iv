﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_WEEKLY_REWARD_LIST_NOT : PacketSend
    {
        public EVENT_WEEKLY_REWARD_LIST_NOT() : base((short) Opcodes.EVENT_WEEKLY_REWARD_LIST_NOT)
        {
            Write.HexArray("00 00 00 00");
        }
    }
}