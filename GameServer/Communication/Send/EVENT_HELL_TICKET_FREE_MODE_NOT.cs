﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_HELL_TICKET_FREE_MODE_NOT : PacketSend
    {
        public EVENT_HELL_TICKET_FREE_MODE_NOT() : base((short) Opcodes.EVENT_HELL_TICKET_FREE_MODE_NOT)
        {
            Write.Int(1);
        }
    }
}