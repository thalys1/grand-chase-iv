﻿using Commons.Packet;
using Networking.Packet;

namespace GameServer.Communication.Send
{
    public class EVENT_FULL_LOOK_INFO_NOT : PacketSend
    {
        public EVENT_FULL_LOOK_INFO_NOT() : base((short) Opcodes.EVENT_FULL_LOOK_INFO_NOT)
        {
            Write.HexArray("00 00 00 00 00 05 7E 2C 00 00 01 90");
        }
    }
}