using Commons.Packet;
using GameServer.Game.Channels.Rooms;
using GameServer.Game.Channels.Rooms.Slots;
using GameServer.Game.Players;

namespace GameServer.Communication.Serializables.Rooms
{
    public static class Rooms
    {
        
        public static void SerializeRoomInfo(Player player, PacketWrite packetWrite)
        {
            var room = player.Room;
            packetWrite.UShort((ushort) room.Id);
            packetWrite.UnicodeStr(room.Name);
            packetWrite.Bool(room.Public);
            packetWrite.Bool(room.Guild);
            packetWrite.UnicodeStr(room.Password);
            packetWrite.Short((short) room.Users);
            packetWrite.Short(7);
            packetWrite.Bool(room.Playing);
            packetWrite.Byte((byte) room.Grade);
            packetWrite.Byte((byte) room.Category);
            packetWrite.Int(room.Mode);
            packetWrite.Int(room.SubMode);
            packetWrite.Bool(room.Random);
            packetWrite.Int(room.MapId);
            packetWrite.Int(room.IP2PVersion);
            foreach (var slot in room.Slots)
            {
                packetWrite.Bool(slot.Open);
            }
            if(room.MaxUsers == 4)
            {
                packetWrite.Bool(false);
                packetWrite.Bool(false);
            }
            packetWrite.Int(room.VecMonsterSlot);
            packetWrite.Int(room.Difficulty);
            packetWrite.HexArray("00 00 00 00 01");
            packetWrite.WriteIP(Emulator.Settings.Network.Host);
            packetWrite.UShort((ushort) Emulator.Settings.UdpRelay);
            packetWrite.WriteIP(Emulator.Settings.Network.Host);
            packetWrite.UShort((ushort) Emulator.Settings.TcpRelay);
        }

        public static void SerializeRoomInfo(Player player, PacketRead packetRead)
        {
            var roomId = (int) packetRead.Short();
            var roomName = packetRead.UnicodeString();
            var roomPublic = packetRead.Bool();
            var roomGuild = packetRead.Bool();
            var roomPassword = packetRead.UnicodeString();
            var users = packetRead.Short();
            var maxUsers = (int) packetRead.Short();
            var playing = packetRead.Bool();
            //var grade = (int) packetRead.Byte();
            var category = (int) packetRead.Byte();
            var gameMode = packetRead.Int();
            var subGameMode = packetRead.Int();
            var randomMap = packetRead.Bool();
            var mapId = packetRead.Int();
            var iP2PVersion = packetRead.Int();
            var slotsOpen = packetRead.Buffer_Array_Bytes(6);
            var vecMonsterSlot = packetRead.Int();
            var difficulty = packetRead.Int();
            packetRead.Jump(17);
            var room = new Room
            {
                Id = (short) roomId,
                Name = roomName,
                Public = roomPublic,
                Guild = roomGuild,
                Password = roomPassword,
                MaxUsers = maxUsers,
                Playing = playing,
                //Grade = grade,
                Category = category,
                Mode = gameMode,
                SubMode = subGameMode,
                Random = randomMap,
                MapId = mapId,
                IP2PVersion = iP2PVersion,
                VecMonsterSlot = vecMonsterSlot,
                Difficulty = difficulty
            };
            room.AddSlot(0, new Slot()
            {
                Active = true,
                PlayerChannel = player,
                Leader = true,
                Open = false,
                Status = 0,
                AwayFromKeyBoard = false,
                Team = 0
                
            });
            for (var i = 1; i < room.MaxUsers; i++)
            {
                room.AddSlot(i, new Slot()
                {
                    Active = false,
                    PlayerChannel = null,
                    Leader = false,
                    Open = true,
                    Status = 0,
                    AwayFromKeyBoard = false,
                    Team = 0

                });
            }
            player.SetRoom(room);
        }
    }
}