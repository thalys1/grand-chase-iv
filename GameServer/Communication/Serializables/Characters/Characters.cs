using System.Collections.Generic;
using Commons.Database.Data.UsersCharacters;
using Commons.Packet;

namespace Commons.Serializables.Characters
{
    public static class Characters
    {
        public static void SerializeVerify(PacketWrite packetWrite, ICollection<UsersCharacterModel> characters)
        {
            packetWrite.Int(characters.Count);
            foreach (var character in characters)
            {
                packetWrite.Byte((byte) character.CharacterType);
                packetWrite.Byte((byte) character.CharacterType);
                packetWrite.Int(0);
                packetWrite.Byte((byte) character.Promotion);
                packetWrite.Byte((byte) character.Promotion);
                packetWrite.Long(character.Experience);
                packetWrite.Int(character.Wins);
                packetWrite.Int(character.Losses);
                packetWrite.Int(character.Wins);
                packetWrite.Int(character.Losses);
                packetWrite.Long(character.Experience);
                packetWrite.Int(character.Level);
                packetWrite.Int(0); // equipamentos
                //foreach

                packetWrite.Int(character.Pet > 0 ? 1 : 0); // has pet
                packetWrite.Int(character.Pet); // pet id
                packetWrite.Int(character.SP); // sp left
                packetWrite.HexArray(
                    "00 00 00 A0 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 64 00 00 00 00 00 00 00 64");
                if (character.SecondWeapon > 0)
                {
                    packetWrite.Short((short) character.SecondWeaponId);
                    packetWrite.Int(1);
                    packetWrite.Short((short) character.SecondWeaponUId);
                    packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                }
                else
                {
                    packetWrite.HexArray(
                        "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                }

                packetWrite.HexArray(
                    "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 2C 00 00 01 2C 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 07");
            }
        }
    }
}