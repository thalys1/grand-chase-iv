using System.Collections.Generic;
using Commons.Database.Data.UsersQuests;
using Commons.Packet;

namespace Commons.Serializables.Quests
{
    public static class Quests
    {
        public static void Serialize(PacketWrite packetWrite, ICollection<UsersQuestModel> quests)
        {
            packetWrite.Int(quests.Count);
            foreach (var quest in quests)
            {
                packetWrite.Int(quest.QuestId);
                packetWrite.Int(1);
                packetWrite.Int(quest.QuestUId);
                packetWrite.Int(quest.Progress);
                packetWrite.HexArray("58 2A 8A 98 58 29 39 18 00 00 00 00");
            }
        }
    }
}