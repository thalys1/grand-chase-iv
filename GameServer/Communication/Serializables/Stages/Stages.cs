using System;
using System.Collections.Generic;
using System.Linq;
using Commons.Database.Data.UsersSage;
using Commons.Packet;

namespace Commons.Serializables.Stages
{
    public static class Stages
    {

        public static void Serialize(PacketWrite packetWrite, IList<UsersStageModel> stages)
        {
            packetWrite.Int(stages.Count());
            foreach (var stage in stages)
            {
                Console.WriteLine(stage.ModeId);
                packetWrite.Int(stage.ModeId);
                packetWrite.HexArray("00 00 00");
                packetWrite.Byte((byte) stage.DifficultyLevelOne);
                packetWrite.Byte((byte) stage.DifficultyLevelTwo);
                packetWrite.Byte((byte) stage.DifficultyLevelThree);
                packetWrite.HexArray("00 00 00 00 00 00");
            }
        }
    }
}