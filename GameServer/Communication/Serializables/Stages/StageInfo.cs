namespace Commons.Serializables.Stages
{
    public class StageInfo
    {
        public int ModeId { get; set; }
        public int DifficultyLevelOne { get; set; }
        public int DifficultyLevelTwo { get; set; }
        public int DifficultyLevelThree { get; set; }
    }
}