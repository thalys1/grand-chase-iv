using Commons.Database.Data.UsersCharacters;
using Commons.Packet;
using GameServer.Game.Players;
using Networking.Channels;

namespace Commons.Serializables.Users
{
    public static class Users
    {
        public static void SerializeUserInfo(Player player, PacketWrite packetWrite)
        {
            packetWrite.UnicodeStr(player.Username);
            packetWrite.Int(player.UsersModel.Id);
            packetWrite.UnicodeStr(player.UsersModel.Username);
            packetWrite.Int(0);
            packetWrite.Byte((byte) player.CharType);
            packetWrite.HexArray("00 00 00 00 00 FF 00 FF 00 FF 00 00 00 00 00 00 00 00 00 64 00 00");
            packetWrite.Int(player.UsersModel.GamePoints);
            packetWrite.Short(0);
        }
        
        public static void SerializeUserInfo(Player player, PacketRead packetRead)
        {
            var strLogin = packetRead.UnicodeString();
            var dwId = packetRead.Int();
            var strNickName = packetRead.UnicodeString();
            packetRead.Int();
            var cStatus = packetRead.Byte();
            packetRead.Jump(22);
            var iGamePoint = packetRead.Int();
            packetRead.Short();
            player.CharType =  cStatus;
        }
        
        public static void SerializeCharacterInfo(Player player, PacketWrite packetWrite)
        {
            var room = player.Room;
            packetWrite.Int(player.UsersModel.Characters.Count);
            foreach (var character in player.UsersModel.Characters)
            {
                packetWrite.Byte((byte) character.CharacterType);
                packetWrite.Int(0);
                packetWrite.Byte((byte) character.Promotion);
                packetWrite.Byte((byte) character.Promotion);
                packetWrite.Long(character.Experience);
                packetWrite.Int(character.Level);
                packetWrite.Int(character.Wins);
                packetWrite.Int(character.Losses);
                packetWrite.Int(0); //packetWrite.Int(character.Equipements.Count);
                //for(int y = 0; y < right.PCharacters.CharInfo[x].Equipements.Count; y++){
                //    packetWrite.Int(right.PCharacters.CharInfo[x].Equipements[y].ItemID);
                //    packetWrite.Int(1);
                //    packetWrite.Int(right.PCharacters.CharInfo[x].Equipements[y].ItemUID);
                //    packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                //}
                packetWrite.Int(0);
                packetWrite.Int(0); // packetWrite.Int(right.PCharacters.CharInfo[x].LookItens.Count);
                //for(int y = 0; y < right.PCharacters.CharInfo[x].LookItens.Count; y++){
                //    packetWrite.UInt(right.PCharacters.CharInfo[x].LookItens[y].ItemID);
                //    packetWrite.Int(1);
                //    packetWrite.UInt(right.PCharacters.CharInfo[x].LookItens[y].ItemUID);
                //    packetWrite.HexArray("00 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                //}
                SerializePetInfo(player, packetWrite, character.CharacterType);
                packetWrite.HexArray("00 00 00 00 00 00 00 00 04 00 00 00 00 00 01 00 00 00 00 02 00 00 00 00 03");
                packetWrite.Int(0);
                packetWrite.HexArray("00 00 00 8C 00 00 00 A0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                packetWrite.Int(character.Promotion + 2);
                packetWrite.Byte((byte) character.CharacterType);
                packetWrite.Byte(255);
                packetWrite.Int(0);
                for(byte y = 0; y < character.Promotion + 1; y++){
                    packetWrite.Byte((byte) character.CharacterType);
                    packetWrite.Byte(y);
                    packetWrite.Int(0);
                }
                packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
                packetWrite.HexArray("00 00 04 E2 00 00 04 E2 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 07");
                packetWrite.Int(character.Id);
                packetWrite.Int(player.UsersModel.GamePoints);
                packetWrite.Int(player.UsersModel.LifeBonus);
                packetWrite.HexArray("00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
            }
        }

        public static void SerializePetInfo(Player player, PacketWrite packetWrite, int characterType)
        {
            packetWrite.Int(0);//Pet is New         
            packetWrite.Int(player.Pets[0].m_dwUID);
            packetWrite.Int(player.Pets[0].m_dwID);
            packetWrite.Str(player.Pets[0].m_strName);
            packetWrite.Int(player.Pets[0].m_mapInitExp);
            for(byte y = 0; y < player.Pets[0].m_mapInitExp; y += 1){
                packetWrite.Byte(y);
                packetWrite.Int(100);
            }
            packetWrite.UInt((uint) player.Pets[0].m_dwEXP);
            packetWrite.Int(player.Pets[0].m_iLevel);
            packetWrite.Byte((byte) player.Pets[0].m_cPromotion);
            packetWrite.Int(player.Pets[0].m_nHatchingID);
            packetWrite.Int(player.Pets[0].m_iInitSatiation);
            packetWrite.Int(player.Pets[0].m_iSatiation);
            packetWrite.Int(player.Pets[0].Slot1.Count);//00 00 00 01
            for(var y = 0; y < player.Pets[0].Slot1.Count; y += 1){
                packetWrite.Int(player.Pets[0].Slot1[y].AtkID);
                packetWrite.Int(1);
                packetWrite.UInt(player.Pets[0].Slot1[y].AtkUID);
                packetWrite.Byte(0);
            }
            packetWrite.Int(player.Pets[0].Slot2.Count);//00 00 00 01
            for(var y = 0; y < player.Pets[0].Slot2.Count; y += 1){
                packetWrite.Int(player.Pets[0].Slot2[y].AtkID);
                packetWrite.Int(1);
                packetWrite.UInt(player.Pets[0].Slot2[y].AtkUID);
                packetWrite.Byte(0);
            }
            packetWrite.HexArray("00 00 00 00 00 00 00 00 00 00 00 00 FF");
            packetWrite.Byte((byte) characterType);
        }
    }
}