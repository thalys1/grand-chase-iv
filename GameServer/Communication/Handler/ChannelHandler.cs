using System;
using System.Threading;
using Commons.Log;
using Commons.Packet;
using GameServer.Communication.Send;
using GameServer.Game;
using Networking;
using Networking.Channels;
using Networking.Packet;

namespace GameServer.Communication.Handler
{
    public class ChannelHandler : INetworkHandler
    {
        public void OnChannelConnected(SessionChannel sessionChannel)
        {
            var cryptography = sessionChannel.Cryptography;
            var cryptoKey = cryptography.RNGCryptoServiceProvider();
            var cryptoAuth = cryptography.RNGCryptoServiceProvider();
            sessionChannel.Send(new EVENT_SET_SECURITY_KEY_NOT(cryptoAuth, cryptoKey));
            cryptography.AuthenticKey = cryptoAuth;
            cryptography.CryptographyKey = cryptoKey;
            sessionChannel.Send(new ENU_WAIT_TIME_NOT(100));
        }

        public void OnChannelDisconnected(SessionChannel sessionChannel)
        {
            NetworkManager.Remove(sessionChannel);
            PlayersManager.TryRemove(sessionChannel?.PlayerChannel);
            WriteConsole.Info("A channel disconnected");
        }

        public void OnChannelDataReceived(int packetId, PacketRead packetRead, SessionChannel sessionChannel)
        {
            NetworkManager.Add(sessionChannel);
            if (PacketManager.TryRequest(packetId, out var packetRequest))
                packetRequest.Dispatch(sessionChannel, packetRead);
            else
                WriteConsole.Warning($"Packet not found with id: {packetId}");

            WriteConsole.Info($"Thread: {Thread.CurrentThread.ManagedThreadId}");
        }

        public void OnChannelException(Exception exception, SessionChannel sessionChannel)
        {
            WriteConsole.Warning($"A channel exception was caught {exception}");
        }
    }
}