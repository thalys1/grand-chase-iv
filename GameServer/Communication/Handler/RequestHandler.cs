using GameServer.Communication.Requests;
using Networking.Packet;

namespace GameServer.Communication.Handler
{
    public class RequestHandler : PacketHandler
    {
        public RequestHandler()
        {
            Register(new EVENT_AGIT_MAP_CATALOGUE_REQ());
            Register(new EVENT_AGIT_STORE_CATALOG_REQ());
            Register(new EVENT_AGIT_STORE_MATERIAL_REQ());
            Register(new EVENT_CHANGE_SLOT_EQUIP_REQ());
            Register(new EVENT_CHANNEL_LIST_REQ());
            Register(new EVENT_CHAR_SELECT_JOIN_REQ());
            Register(new EVENT_CHOICE_BOX_LIST_REQ());
            Register(new EVENT_COST_RATE_FOR_GAMBLE_BUY_REQ());
            Register(new EVENT_CREATE_ROOM_REQ());
            Register(new EVENT_DEPOT_INFO_REQ());
            Register(new EVENT_DONATION_INFO_REQ());
            Register(new EVENT_ENTER_CHANNEL_REQ());
            Register(new EVENT_EXP_POTION_LIST_REQ());
            Register(new EVENT_FAIRY_TREE_LV_TABLE_REQ());
            Register(new EVENT_GET_FULL_SP_INFO_REQ());
            Register(new EVENT_GET_USER_DONATION_INFO_REQ());
            Register(new EVENT_INVEN_BUFF_ITEM_LIST_REQ());
            Register(new EVENT_INVITE_DENY_NOT());
            Register(new EVENT_LEAVE_CHANNEL_NOT());
            Register(new EVENT_PET_COSTUM_LIST_REQ());
            Register(new EVENT_RECOMMEND_FULL_INFO_REQ());
            Register(new EVENT_ROOM_LIST_REQ());
            Register(new EVENT_SERVER_LIST_REQ());
            Register(new EVENT_SET_IDLE_STATE_REQ());
            Register(new EVENT_STAT_CLIENT_INFO());
            Register(new EVENT_USER_BINGO_DATA_REQ());
            Register(new EVENT_VERIFY_ACCOUNT_REQ());
            Register(new HEART_BIT_NOT());
        }
    }
}