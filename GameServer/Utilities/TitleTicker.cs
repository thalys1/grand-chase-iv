using System;
using System.Threading;
using GameServer.Game;
using GameServer.Game.Players;
using Networking;

namespace GameServer.Utilities
{
    public static class TitleTicker
    {
        private static Timer _timer;

        public static void Build()
        {
            if (_timer == null)
                _timer = new Timer(
                    state =>
                    {
                        Console.Title =
                            $"Game Server | Channels: {ChannelManager.Count} | Sessions: {NetworkManager.Count} | Players: {PlayersManager.Count}";
                    }, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        public static void Dispose()
        {
            _timer?.Dispose();
            _timer = null;
        }
    }
}