using System.Collections.Concurrent;
using Commons.Log;
using GameServer.Game.Channels;
using Networking.Channels;

namespace GameServer.Game
{
    public static class ChannelManager
    {
        private static readonly ConcurrentDictionary<int, Channel> Channels = new ConcurrentDictionary<int, Channel>();

        public static int Count => Channels.Count;

        public static bool Enter(int channelId, PlayerChannel playerChannel)
        {
            if (!Channels.TryGetValue(channelId, out var channel)) return false;
            if (!channel.AddSession(playerChannel)) return false;
            return true;
        }

        public static void Add(Channel channel)
        {
            if (!Channels.TryAdd(channel.Id, channel))
                WriteConsole.Warning($"Can't add channel [{channel.Id}]: {channel.Name}");
        }

        public static Channel Get(int channelId)
        {
            return Channels.TryGetValue(channelId, out var channel) ? channel : null;
        }
    }
}