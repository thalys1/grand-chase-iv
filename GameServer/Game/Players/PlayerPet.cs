using System.Collections.Generic;

namespace GameServer.Game.Players
{
    public class PlayerPet
    {
        public int m_dwUID { get; set; }
        public int m_dwID { get; set; }
        public string m_strName { get; set; }
        public int m_mapInitExp { get; set; }
        public int m_dwEXP { get; set; }
        public int m_iLevel { get; set; }
        public int m_cPromotion { get; set; }
        public int m_nHatchingID { get; set; }
        public int m_iInitSatiation { get; set; }
        public int m_iSatiation { get; set; }
        public List<PlayerPetAttack> Slot1 { get; set; }
        public List<PlayerPetAttack> Slot2 { get; set; }
    }
}