using System.Collections.Generic;
using GameServer.Game.Channels;
using GameServer.Game.Channels.Rooms;
using Networking.Channels;

namespace GameServer.Game.Players
{
    public class Player : PlayerChannel
    {
        public Channel CurrentChannel { get; set; }
        
        public Room Room { get; private set; }
        
        public List<PlayerPet> Pets = new List<PlayerPet>()
        {
            new PlayerPet()
            {
                m_dwUID = 0,
                m_dwID = 0,
                m_strName = "",
                m_mapInitExp = 0,
                m_dwEXP = 0,
                m_iLevel = 0,
                m_cPromotion = 0,
                m_nHatchingID = 0,
                m_iInitSatiation = 0,
                m_iSatiation = 0,
                Slot1 = new List<PlayerPetAttack>(),
                Slot2 = new List<PlayerPetAttack>(),
            }
        };
        
        public override bool EnterChannel(int channelId)
        {
            if (!ChannelManager.Enter(channelId, this)) return false;
            CurrentChannel = ChannelManager.Get(channelId);
            return true;

        }

        public override void SetRoom(object room)
        {
            Room = (Room) room;
        }
        
        public override void LeaveChannel()
        {
            CurrentChannel?.RemoveSession(this);
        }
    }
}