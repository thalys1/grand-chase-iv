using System.Collections.Concurrent;
using Networking.Channels;

namespace GameServer.Game
{
    public static class PlayersManager
    {
        private static readonly ConcurrentDictionary<string, PlayerChannel> Players =
            new ConcurrentDictionary<string, PlayerChannel>();

        public static int Count => Players.Count;

        public static bool TryAdd(PlayerChannel session)
        {
            return Players.TryAdd(session.Username, session);
        }

        public static bool TryRemove(PlayerChannel session)
        {
            return Players.TryRemove(session.Username, out session);
        }
    }
}