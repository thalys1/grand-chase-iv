﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using GameServer.Game.Channels.Rooms.Slots;
using Networking.Channels;

namespace GameServer.Game.Channels.Rooms
{
    public class Room
    {
        private readonly ConcurrentDictionary<string, PlayerChannel> _sessions = new ConcurrentDictionary<string, PlayerChannel>();

        public readonly Dictionary<int, Slot> _slots = new Dictionary<int, Slot>();

        public Slot[] Slots => _slots.Values.ToArray();

        public int Users => _sessions.Count;

        public int Id { get; set; }
        
        public int MaxUsers { get; set; }
        
        public int Grade { get; set; }
        
        public int Category { get; set; }
        
        public int Mode { get; set; }
        
        public int SubMode { get; set; }
        
        public int MapId { get; set; }
        
        public int IP2PVersion { get; set; }
        
        public int VecMonsterSlot { get; set; }
        
        public int Difficulty { get; set; }
        
        public string Name { get; set; }
        
        public string Password { get; set; }
        
        public bool Public { get; set; }
        
        public bool Guild { get; set; }
        
        public bool Playing { get; set; }
        
        public bool Random { get; set; }

        public void AddSlot(int id, Slot slot)
        {
            _slots.Add(id, slot);
        }

        public int SlotsOpen
        {
            get
            {
                {
                    return Slots.Count(x => x.Open);
                }
            }
        }

        public PlayerChannel Leader
        {
            get { return Slots.SingleOrDefault(x => x.Leader)?.PlayerChannel; }
        }
    }
}