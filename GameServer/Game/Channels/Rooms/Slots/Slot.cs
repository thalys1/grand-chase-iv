﻿using Networking.Channels;

namespace GameServer.Game.Channels.Rooms.Slots
{
    public class Slot
    {
        public bool Active { get; set; }
        public bool AwayFromKeyBoard{ get; set; }
        public bool Leader { get; set; }
        public int Status { get; set; }
        public bool Open { get; set; }
        public PlayerChannel PlayerChannel { get; set; }
        public int Team { get; set; }
        public byte State { get; set; }
    }
}