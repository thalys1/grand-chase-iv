﻿using System.Collections.Concurrent;
using GameServer.Game.Channels.Rooms;
using Networking.Channels;

namespace GameServer.Game.Channels
{
    public class Channel
    {
        private readonly ConcurrentDictionary<int, Room> _rooms = new ConcurrentDictionary<int, Room>();

        private readonly ConcurrentDictionary<string, PlayerChannel> _sessions = new ConcurrentDictionary<string, PlayerChannel>();
        
        public int Id { get; set; }

        public int Limit { get; set; }

        public int Level { get; set; }

        public string Name { get; set; }

        public int Sessions => _sessions.Count;

        public int Rooms => _rooms.Count;

        public bool AddSession(PlayerChannel playerChannel)
        {
            return _sessions.TryAdd(playerChannel.Username, playerChannel);
        }

        protected internal bool RemoveSession(PlayerChannel playerChannel)
        {
            return _sessions.TryRemove(playerChannel.Username, out playerChannel);
        }
    }
}